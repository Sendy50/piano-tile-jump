#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("k+wYQDqaZpFKsRKWL2nv+gWkQu6oaELFv1hjYaM4+FkGkzuS/9xwuiQZjK4sNmlwhL3YakDFOWGnnjeEHd+5m40bMj0eV9ZirZTBzXz5y1QXzaFvdSXCXxLoAGTVuxD3idBjFNRc4W92vBueHb+bAPW1Fvg3qnKiw5jjDLJ8A3aJZQU3A2IW0d3TUlrbhtLZ1Tv7WpXFktWL9Vzk0x4hDGXXVHdlWFNcf9Md06JYVFRUUFVW6W+aaIRDXxC37RHPRkhhQeiG8WV3pAZMvn4QON2ACPqp9SG7aS622tdUWlVl11RfV9dUVFXv8Mq6zwprHGmOeVOjQKo2feMcOPoUigwdlWGWnRh/sAJAbqnYnZyEc4QzoSE4yVp56+QtnfFyAldWVFVU");
        private static int[] order = new int[] { 3,10,11,9,11,7,7,12,9,9,13,13,12,13,14 };
        private static int key = 85;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
