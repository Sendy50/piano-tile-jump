﻿
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
//#if AADOTWEEN
using DG.Tweening;
using TMPro;
using UnityEngine.SceneManagement;
using EasyMobile;
//#endif    

#if APPADVISORY_ADS
using AppAdvisory.Ads;
#endif

namespace AppAdvisory.Boing
{
    public class GameManager : MonoBehaviour
    {
        public int numberOfPlayToShowInterstitial = 5;

        public static GameManager gameManagerRef;

        public string VerySimpleAdsURL = "http://u3d.as/oWD";

        #region canvas
        public Text titleText;
        public Text bestScoreText;
        public Text pointText;
        public Text BoostModeText;
        public GameObject LevelNamePanel;
        public TMP_Text diamondText;
        public TMP_Text KeyText;
        public GameObject tutoAtStart;
        #endregion

        #region player
        public GameObject player;
        public Transform playerParent;
        public Transform playerSphere;
        public Transform shadow;
        #endregion

        #region customizable variables
        public List<Color> colors;
        public float distanceZ = 5;
        public int numberOfPlatformToSpawnedAtStart = 6;
        public int changeColorEveryXPoints = 15;
        public int BoostMode = 0;
        public int LocalBoostMode = 1;
        public float speedBounceInSeconds; //start set 0.4f
        public ParticleSystem LvlUpParticals;
        public GameObject BGSprite;
        private float boostHighSpeed;
        public float boostLowSpeed; //start set 0.3f for Boost 0
        private int RandomChangBoostPoint4;
        private int RandomChangBoostPoint3;
        private int RandomChangBoostPoint2;
        private int RandomChangBoostPoint;
        float swipeSensibility = 2f;//7f
        private bool gameStart = false;


        public Image ReliveAdsIcont;
        #endregion

        #region public variables
        public int point = 0;
        public int diamond = 0;
        public int key = 0;
        #endregion

        #region references

        public GameObject platformPrefab;
        public Material plateformMaterial;
        #endregion

        #region sounds
        AudioSource audioSource;
        AudioSource audioSource2;
        public AudioClip BgSoundClip;
        public AudioClip soundJump;
        public AudioClip CoinDropClick;

        public void PlaySoundJump()
        {
            if (PlayerPrefs.GetInt("Sound", 1) == 1)
            {
                audioSource.volume = 0.7f;
                audioSource.PlayOneShot(soundJump);
            }
        }
        public AudioClip soundFall;
        void PlaySoundFall()
        {
            if (PlayerPrefs.GetInt("Sound", 1) == 1)
            {
                audioSource.PlayOneShot(soundFall);
            }
        }
        public AudioClip soundDiamond;
        public AudioClip soundKey;
        void PlaySoundDiamond()
        {
            if (PlayerPrefs.GetInt("Sound", 1) == 1)
            {
                audioSource.volume = 0.4f;
                audioSource.PlayOneShot(soundDiamond);
            }
        }
        void PlaySoundKey()
        {
            if (PlayerPrefs.GetInt("Sound", 1) == 1)
            {
                audioSource.volume = 0.4f;
                audioSource.PlayOneShot(soundKey);
            }
        }

        public void CoinDropSound()
        {
            if (PlayerPrefs.GetInt("Sound", 1) == 1)
            {
                audioSource.PlayOneShot(CoinDropClick);
            }
        }
        #endregion

        bool gameIsStarted = false;
        bool isGameOver = false;
        public int spawnPlatformCount = 0;
        float posZTarget = 0f; //-0.15f;//.5f , //0f
        public Camera camGame;


        private bool SpeedBoolLow = true;
        private bool SpeedBoolHigh = false;
        private Animator animator;
        private float Distance = 5f;



        private float ExtraBallDistance = 0f;
        private float TempBounceSpeed;
        public float TileposZTarget;
        public int keyTempPoint;
        private int UseKey = 2;

        #region OutScreen Variables
        public GameObject PausePanel;
        public GameObject GiftScreenCanvas;
        public GameObject OutScreenCan;
        public GameObject AddGiftPanel;

        public TMP_Text TotalDiamondText;
        public TMP_Text TotalKeyText;
        public TMP_Text LevelNameText;
        public Image BaseImage;
        public Image BaseImage1;
        public Image BaseImage2;
        public Image BaseImage3;

        public TMP_Text ScoreText;
        public TMP_Text HighestScoreText;
        public TMP_Text GotDiamondInGameText;
        public TMP_Text GotKeyInGameText;
        public Button X4Button;
        #endregion

        #region PreOutScreenCanvas
        public GameObject PreOutScreenCanvas;
        public GameObject MinusKeyPanel;
        public TMP_Text KeyMinusText;
        public GameObject AdCOuntDownPanel;
        public TMP_Text CountDownText;
        #endregion




        public bool JumpPower;
        public bool JumppedPower = false;
        public int JumpCount = 0;
        private float currCountdownValue;
        private bool BoolMinusKeyPanel;
        private bool BoolAdCountDownPanel;
        private bool ReLivee = false;
        private Coroutine countDownCoroutine;
        private bool JumpPower2;
        private Texture2D SStexture;
        internal int TempCount = 0;


        //new Touch
        private Vector2 leftFingerPos;
        private Vector2 leftFingerLastPos;
        private Vector2 leftFingerMovedBy;
        private float slideMagnitudeX;

        private float pressX;
        private float velocity;
        private float x;
        private bool firstPlay = true;




        void Awake()
        {
            //audioSource2.PlayScheduled(BgSoundClip);
            audioSource = GetComponent<AudioSource>();
            gameManagerRef = this;

            Time.fixedDeltaTime = 1f / 60f;
            Time.maximumDeltaTime = 5f / 60f;

            var go = GameObject.Find("[DOTween]");

            if (go == null)
            {
                //#if AADOTWEEN
                DOTween.Init();
                //#endif
            }
        }

        IEnumerator Start()
        {



            bestScoreText.text = Util.GetBestScore().ToString();
            pointText.gameObject.SetActive(false);
            titleText.text = Util.GetLevelName();

            tutoAtStart.SetActive(true);
            diamond = 0;
            key = 0;
            diamondText.text = Util.GetDiamond().ToString(); // diamond.ToString()
            KeyText.text = Util.GetKey().ToString(); //key.ToString()

            keyTempPoint = (int)Util.GetRandomNumber(60, 90);

            // animator.SetInteger("levelEnvironment", levelEnvironment);

            for (int i = 0; i < numberOfPlatformToSpawnedAtStart; i++)
            {
                SpawnPlatform();
            }

            while (true)
            {
                if (Input.GetMouseButton(0))
                {

                    if (!gameIsStarted)
                        StartTheGame();
                    break;
                }

                yield return 0;
            }


        }


        void StartTheGame()
        {
            if (!gameIsStarted)
            {
                PlayerMove();

                //Show banner ad
                print("Show Banner ... ");
                AdsManagerForLevel.Instance.ShowBannerAds();

                gameIsStarted = true;
                gameStart = true;
                firstPlay = false;
                SoundManagerr.Instance.PlayBgMusicInLoop();
            }

            pointText.gameObject.SetActive(true);
            //bestScoreText.gameObject.SetActive(false);
            //titleText.gameObject.SetActive(false);
            LevelNamePanel.gameObject.SetActive(false);
            tutoAtStart.SetActive(false);
        }

        private void Update()
        {


            if (isGameOver)
            {
                if (PausePanel.activeInHierarchy)
                {
                    PausePanel.SetActive(false);
                    Time.timeScale = 1;
                }
                return;
            }


            if (Application.isMobilePlatform)
            {
                // If pressed with one finger
                if (Input.GetMouseButtonDown(0))
                {
                    PauseGame(false);
                    firstPlay = false;
                    pressX = Input.touches[0].position.x;
                }
                else if (Input.GetMouseButtonUp(0))
                {
                    pressX = 0;
                }

                if (pressX != 0)
                {
                    float currentX = Input.touches[0].position.x;

                    // The finger of initial press is now left of the press position
                    if (currentX < pressX)
                        Move((currentX - pressX) / 100);

                    // The finger of initial press is now right of the press position
                    else if (currentX > pressX)
                        Move((currentX - pressX) / 100);
                }
                else
                {
                    if (!firstPlay)
                        PauseGame(true);
                }
            }
            else
            {
                if (Input.GetMouseButtonDown(0))
                {
                    PauseGame(false);
                    firstPlay = false;
                    pressX = Input.mousePosition.x;
                }
                else if (Input.GetMouseButtonUp(0))
                    pressX = 0;


                if (pressX != 0)
                {
                    float currentX = Input.mousePosition.x;

                    // The finger of initial press is now left of the press position
                    if (currentX < pressX)
                        Move((currentX - pressX) / 100);

                    // The finger of initial press is now right of the press position
                    else if (currentX > pressX)
                        Move((currentX - pressX) / 100);
                }
                else
                {
                    if (!firstPlay)
                        PauseGame(true);
                }
            }
        }

        private void Move(float velocity)
        {
            if (!gameIsStarted)
            {
                return;
            }

            swipeSensibility = PlayerPrefs.GetFloat("swipeSensibility", 0.5f) + 1f;
            print("swipeSensibility " + swipeSensibility);

            var playerPos = playerParent.position;
            playerPos.x = Mathf.Lerp(playerPos.x, Mathf.Clamp(velocity * swipeSensibility, -2.5f, 2.5f), 10f * Time.deltaTime);
            playerParent.position = playerPos;

            SoundManagerr.Instance._BgMusicSource.panStereo = Mathf.Clamp((float)(playerPos.x / 3), -0.9f, 0.9f);
        }




        public void PauseGame(bool pause)
        {
            if (!isGameOver)
            {
                PausePanel.SetActive(pause);
                if (pause)
                {
                    //tutoAtStart.SetActive(false);
                    tutoAtStart.SetActive(true);
                    Time.timeScale = 0;
                }
                else
                {
                    tutoAtStart.SetActive(false);
                    Time.timeScale = 1;
                }
            }

        }

        void UpdateCamPosZ()
        {
            var pCam = camGame.transform.position;
            pCam.z = playerParent.position.z - 8.63f;
            camGame.transform.position = pCam;

            pCam = camGame.transform.position;
            pCam.z = playerParent.position.z - 8.63f;
            camGame.transform.position = pCam;
        }

        Ease playerAnimEase = Ease.OutQuad; //Ease.OutCubic; //SetEase(Ease.OutQuad);   


        void PlayerMove()
        {
            if (ReLivee)
            {
                ReLivee = false;
                Platform.Instance.OnPlayerBounce();
                Add1Point();
            }
            else
            {
                CheckIfPlayerIsGrounded();
            }


            if (isGameOver)
                return;


            //Boost Control Code
            if (speedBounceInSeconds <= boostHighSpeed && SpeedBoolHigh)
            {
                speedBounceInSeconds += 0.0025f;
                // print("<<< PLUS >>> point --> " + point + " speedBounceInSeconds --> " + speedBounceInSeconds + "  boostHighSpeed --> " + boostHighSpeed);
                if (speedBounceInSeconds >= boostHighSpeed)
                {
                    SpeedBoolHigh = false;
                    SpeedBoolLow = true;
                }
            }
            else if (speedBounceInSeconds >= boostLowSpeed && SpeedBoolLow)
            {
                speedBounceInSeconds -= 0.003f;
                // print("<<< MINUS >>> point --> " + point + "speedBounceInSeconds --> " + speedBounceInSeconds + "  boostLowSpeed --> " + boostLowSpeed);
                if (speedBounceInSeconds <= boostLowSpeed)
                {
                    SpeedBoolHigh = true;
                    SpeedBoolLow = false;
                }
            }


            AnimShadow();

            if (JumpPower)
            {

                //print("50f jump point ==> " + point + " plateformCount ==> " + spawnPlatformCount);
                ExtraBallDistance = 45f;
                // bool abc = true;
                playerSphere.DOMoveY(94.5f, speedBounceInSeconds * 3.5f).SetLoops(2, LoopType.Yoyo).SetEase(playerAnimEase) //.SetLoops(2, LoopType.Yoyo)          
                    .OnComplete(() =>
                    {
                        JumpCount++;
                    }); //ball hight

            }
            else if ((point) % 13 == 0 && (point) > 6 && !JumpPower)
            {
                ExtraBallDistance = 5f;

                //print("10f jump point ==> " + point + " plateformCount ==> " + spawnPlatformCount);
                playerSphere.DOMoveY(93.0f, speedBounceInSeconds * 1.3f).SetLoops(2, LoopType.Yoyo).SetEase(playerAnimEase); //ball hight        
                //spawnPlatformCount--;
            }
            else
            {
                //print("5f jump point ==> " + point + " plateformCount ==> " + spawnPlatformCount);
                ExtraBallDistance = 0f;
                playerSphere.DOMoveY(92.5f, speedBounceInSeconds).SetLoops(2, LoopType.Yoyo).SetEase(playerAnimEase); //ball hight                                                                                                                     // speedBounceInSeconds = TempBounceSpeed;
            }


            if (Util.GetLevelIndex() == 1)
            {
                playerParent.position = new Vector3(JumpPower ? 0 : playerParent.position.x, 90.30f, posZTarget);
            }
            else
            {
                playerParent.position = new Vector3(JumpPower ? 0 : playerParent.position.x, 90f, posZTarget);
            }


            posZTarget += 5f + ExtraBallDistance; // 5f convert to distance variable for use in ball and tile distance

            //print("<<@@@ posZTarget  = " + posZTarget + " plateformCount ==> " + spawnPlatformCount);

            //print(" jumPower = " + JumpPower + " JumpCount " + JumpCount);



            //#if AADOTWEEN
            playerParent.DOMoveZ(posZTarget, ExtraBallDistance == 0 ? speedBounceInSeconds * 2.0f : JumpPower ? (speedBounceInSeconds * 3.5f) * 2.0f : (speedBounceInSeconds * 1.3f) * 2.0f) // ball distance
                .SetEase(Ease.Linear)
                .OnUpdate(() =>
                {
                    //OnUpdate();
                    UpdateCamPosZ();
                    if (JumpPower)
                    {
                        SoundManagerr.Instance._BgMusicSource.volume = SoundManagerr.Instance._BgMusicSource.volume + 0.005f;
                    }

                })
                .OnComplete(() =>
                {
                    if (JumpCount == 3)
                    {
                        JumpCount = 0;
                        JumpPower = false;
                        JumppedPower = false;
                        SoundManagerr.Instance._BgMusicSource.volume = 0.9f;
                        //point = point - 1;
                    }

                    PlaySoundJump();
                    PlayerMove();
                });
            //#endif
        }

        void AnimShadow()
        {
            if (isGameOver)
                return;

            float shadowLocalScale = 0.08f;
            //#if AADOTWEEN
            shadow.DOScale(0 * shadowLocalScale, speedBounceInSeconds)
                .SetEase(Ease.OutExpo)
                .OnComplete(() =>
                {
                    shadow.DOScale(shadowLocalScale, speedBounceInSeconds)
                        .SetEase(Ease.InExpo);
                });
            //#endif
        }

        void CheckIfPlayerIsGrounded()
        {
            if (isGameOver)
                return;

            if (posZTarget > 4)
            {
                RaycastHit hit;

                Vector3 down = playerSphere.TransformDirection(Vector3.down);

                if (Physics.Raycast(playerSphere.position, down, out hit))
                {
                    Platform platform = hit.transform.parent.GetComponent<Platform>();
                    if (platform != null)
                    {
                        // particals.Play();

                        platform.OnPlayerBounce();
                        Add1Point();
                    }
                }
                else
                {
                    isGameOver = true;
                    PlaySoundFall();

                    playerSphere.DOLocalMoveY(-10, 1).OnComplete(() =>
                    {

                        //Show Interstial Ads Here And Return to Call //ReLiveCheck();
                        SoundManagerr.Instance._BgMusicSource.Stop();
                        // Destroy banner ad
                        print("Hide Banner ... ");
                        AdsManagerForLevel.Instance.HideBannerAds();
                        AdsManagerForLevel.Instance.ShowInterstialAds(0);
                        //ReLiveCheck();
                    });
                    shadow.gameObject.SetActive(false);
                }
            }
        }

        //return From AdsManagerForLevel After Close Interstitial Ads 
        public void ReLiveCheck()
        {
            SoundManagerr.Instance._BgMusicSource.Stop();

            PreOutScreenCanvas.SetActive(true);
            if (Util.GetKey() > UseKey)
            {
                BoolMinusKeyPanel = true;
                BoolAdCountDownPanel = false;
                KeyMinusText.text = UseKey + " Out Of " + Util.GetKey().ToString() + "\n" +
                    "Key use For Relive";
                MinusKeyPanel.SetActive(true);

                // AdsManagerForLevel.Instance.ShowNativeCenter();

                //hide ads icon and set text in center
                //RectTransform trans = ReliveText.GetComponent<RectTransform>();
                //ReliveText.transform.position = new Vector3(0, -3.175f, 0);
                ReliveAdsIcont.gameObject.SetActive(false);
            }
            else
            {
                if (gameStart)
                {
                    BoolAdCountDownPanel = true;
                    BoolMinusKeyPanel = false;
                    AdCOuntDownPanel.SetActive(true);
                    countDownCoroutine = StartCoroutine(StartCountdown());
                    gameStart = false;
                    //Show ads icon and set text in Some right
                    //RectTransform trans = ReliveText.GetComponent<RectTransform>();
                    //ReliveText.transform.position = new Vector3(32f, -3.175f, 0);
                    ReliveAdsIcont.gameObject.SetActive(true);
                }
                else
                {
                    GameOver();
                }

            }
        }

        public IEnumerator StartCountdown(float countdownValue = 5)
        {
            currCountdownValue = countdownValue;
            while (currCountdownValue > 0)
            {
                CountDownText.text = currCountdownValue.ToString();
                Debug.Log("Countdown: " + currCountdownValue);
                yield return new WaitForSeconds(1.0f);
                currCountdownValue--;
            }

            if (currCountdownValue < 1)
            {
                StopCoroutine(countDownCoroutine);
                GameOver();
            }
        }

        public void ReLiveBtnClick() // Also Call From Save By Key Button Without Ad // Ad_id = 5
        {
            PlaySoundJump();
            if (BoolMinusKeyPanel)
            {
                Util.SetKey(Util.GetKey() - UseKey);
                KeyText.text = Util.GetKey().ToString();
                UseKey *= 2;
                ReLive();
            }
            else if (BoolAdCountDownPanel)
            {
                //Show Ad Here After Complete Reward Call Relive method
                StopCoroutine(countDownCoroutine);
                AdsManagerForLevel.Instance.ShowRewardAds(5);
            }
        }

        public void ReturnFromAdReLiveByCountDown()
        {

            ReLive();
        }

        private void ReLive()
        {
            //Relive Code
            GameObject[] objs;
            objs = GameObject.FindGameObjectsWithTag("Plateform");

            foreach (GameObject plateform in objs)
            {
                //plateform.transform.position = new Vector3(0, plateform.transform.position.y, plateform.transform.position.z);
                plateform.transform.DOMoveX(0f, 0.5f);

                if (plateform.transform.GetChild(2).gameObject.activeInHierarchy)
                {
                    //print("transform.position.z - gameManager.camGame.transform.position.z" + (transform.position.z - gameManager.camGame.transform.position.z));
                    Add1Diamond();
                    plateform.transform.GetChild(2).gameObject.SetActive(false);
                }
                else if (plateform.transform.GetChild(4).gameObject.activeInHierarchy)
                {
                    Add1Key();
                    plateform.transform.GetChild(4).gameObject.SetActive(false);
                }
            }
            SoundManagerr.Instance.PlayBgMusicInLoop();

            AdCOuntDownPanel.SetActive(false);
            MinusKeyPanel.SetActive(false);
            PreOutScreenCanvas.SetActive(false);

            ReLivee = true;
            isGameOver = false;
            firstPlay = true;

            //Show banner ad
            print("Show Banner ... ");
            AdsManagerForLevel.Instance.ShowBannerAds();

            shadow.gameObject.SetActive(true);

            playerParent.DOMoveX(0f, 0.5f); //position.x = new Vector3(0, 90f, posZTarget);
            playerSphere.DOLocalMoveY(0f, 1)
            .OnComplete(() =>
            {
                print("Relive >>>>>>>>>  :>)");
                PlayerMove();
            });


            //Relive Achievement
            PlayerPrefs.SetInt("ReliveCounter", (PlayerPrefs.GetInt("ReliveCounter", 0) + 1));
            PlayerPrefs.Save();

            if (PlayerPrefs.GetInt("ReliveCounter", 0) >= 100)
            {
                GameServices.UnlockAchievement(EM_GameServicesConstants.Achievement_AchivementRelive100);
            }
            else if (PlayerPrefs.GetInt("ReliveCounter", 0) >= 50)
            {
                GameServices.UnlockAchievement(EM_GameServicesConstants.Achievement_AchivementRelive50);
            }
            else if (PlayerPrefs.GetInt("ReliveCounter", 0) >= 10)
            {
                GameServices.UnlockAchievement(EM_GameServicesConstants.Achievement_AchivementRelive10);
            }
        }

        public void NoThanksBtnClick()
        {
            //AdsManagerForLevel.Instance.HideNativeAdsCenter();
            GameOver();
        }

        void Add1Point()
        {
            if (isGameOver)
                return;


            if (JumpPower2)
            {
                point = point + 10;
                pointText.text = point.ToString();
            }
            else
            {

                point++;
                pointText.text = point.ToString();
            }

            //if (JumpPower3 && !JumpPower2)
            //{
            //    JumpPower2 = false;
            //}

            if (JumpPower)
            {
                JumpPower2 = true;
            }
            else
            {
                JumpPower2 = false;
            }

            //point++;
            //pointText.text = point.ToString();

            //if(point % 3 == 0)
            //{
            //	colors.Shuffle();

            //	//#if AADOTWEEN
            //	plateformMaterial.DOColor(colors[0], 1);
            //	//#endif
            //}

            // if ((spawnPlatformCount-3) % 7 == 0 && spawnPlatformCount > 6)




            if (point >= UnityEngine.Random.Range(99, 120) && BoostMode == 0)//110  for test //10,20,30,40,50 infinity
            {
                BoostMode = 1;
                speedBounceInSeconds = 0.30f;
                //boostHighSpeed = 0.30f;
                //boostLowSpeed = 0.20f;
                boostHighSpeed = 0.30f;
                boostLowSpeed = 0.26f;
                RandomChangBoostPoint = UnityEngine.Random.Range(199, 210);
                // RandomChangBoostPoint = UnityEngine.Random.Range(30, 32);
                print("Boost 1 set RandomChangBoostPoint --> " + RandomChangBoostPoint);
            }
            else if (point >= RandomChangBoostPoint && BoostMode == 1)
            {
                BoostMode = 2;
                speedBounceInSeconds = 0.28f;
                //boostHighSpeed = 0.28f;
                //boostLowSpeed = 0.19f;
                boostHighSpeed = 0.26f;
                boostLowSpeed = 0.23f;
                RandomChangBoostPoint2 = UnityEngine.Random.Range(300, 320);
                //RandomChangBoostPoint2 = UnityEngine.Random.Range(48, 50);
                print("Boost 2 set RandomChangBoostPoint --> " + RandomChangBoostPoint2);
            }
            else if (point >= RandomChangBoostPoint2 && BoostMode == 2)
            {
                BoostMode = 3;
                speedBounceInSeconds = 0.25f;
                //boostHighSpeed = 0.25f;
                //boostLowSpeed = 0.18f;
                boostHighSpeed = 0.23f;
                boostLowSpeed = 0.19f;
                RandomChangBoostPoint3 = UnityEngine.Random.Range(380, 398);
                // RandomChangBoostPoint3 = UnityEngine.Random.Range(68, 70);
                print("Boost 3 set RandomChangBoostPoint --> " + RandomChangBoostPoint3);
            }
            else if (point >= RandomChangBoostPoint3 && BoostMode == 3)
            {
                BoostMode = 4;
                speedBounceInSeconds = 0.20f;
                //boostHighSpeed = 0.20f;
                //boostLowSpeed = 0.18f;
                boostHighSpeed = 0.19f;
                boostLowSpeed = 0.16f;
                RandomChangBoostPoint4 = UnityEngine.Random.Range(485, 500);
                //RandomChangBoostPoint4 = UnityEngine.Random.Range(98, 100);
                print("Boost 4 set RandomChangBoostPoint --> " + RandomChangBoostPoint4);
            }
            else if (point >= RandomChangBoostPoint4 && BoostMode == 4)
            {
                BoostMode = 5; // infinity
                speedBounceInSeconds = 0.25f;
                boostHighSpeed = 0.25f;
                boostLowSpeed = 0.18f;

                print("Infinity Mode Started set speedBounceInSeconds --> " + speedBounceInSeconds);
            }
        }

        public void Add1Key()
        {
            if (isGameOver)
                return;

            key++;

            Util.SetKey(Util.GetKey() + 1);

            KeyText.text = Util.GetKey().ToString(); // key.ToString();
            PlaySoundKey();
        }

        public void JumpPowered()
        {
            if (isGameOver)
                return;


            JumpPower = true;



            // jumPowerCounter++;
            // point = point + 2;
            PlaySoundKey();
        }

        public void Add1Diamond()
        {
            if (isGameOver)
                return;

            diamond++;

            Util.SetDiamond(Util.GetDiamond() + 1);

            diamondText.text = Util.GetDiamond().ToString(); // diamond.ToString();
            PlaySoundDiamond();
        }

        public void SpawnPlatform()
        {
            if (isGameOver)
                return;

            GameObject go = Instantiate(platformPrefab) as GameObject;

            Transform t = go.transform;

            SpawnPlatform(t);
        }





        public void SpawnPlatform(Transform t)
        {
            if (isGameOver)
                return;

            float posXmax = (point > 300) ? 2f : 2.5f;//3f; //-2.5f;

            if (BoostMode == LocalBoostMode && BoostMode < 5)
            {
                BoostModeText.gameObject.SetActive(true);
                LvlUpParticals.gameObject.SetActive(true);
                String boost = (BoostMode == 4) ? "Infinity" : BoostMode.ToString();
                BoostModeText.text = "Boost Mode " + boost;
                LocalBoostMode++;
                print("bosst mode = " + BoostMode + " localboostmode " + LocalBoostMode);
                //LvlUpParticals.transform.position.z = (BGSprite.transform.position.z - 43.85f);
                LvlUpParticals.transform.position = new Vector3(LvlUpParticals.transform.position.x, LvlUpParticals.transform.position.y, (BGSprite.transform.position.z - 33.85f));
                LvlUpParticals.time = 0;
                LvlUpParticals.Play();
                //StartCoroutine(BoostModeShow());
            }
            else
            {
                if (BoostModeText.IsActive() && BoostMode < 5)
                    StartCoroutine(HideBoostText());
            }


            float posX = Util.GetRandomNumber(-posXmax, posXmax);

            if (Util.GetRandomNumber(0, 100f) < 70f)
            {
                if (Util.GetRandomNumber(0, 100f) < 50f)
                {
                    posX = Util.GetRandomNumber(-posXmax, -1f);
                }
                else
                {
                    posX = Util.GetRandomNumber(1f, posXmax);
                }
            }



            //if (spawnPlatformCount % 13 == 0 && spawnPlatformCount > 6 && !JumppedPower)
            if (spawnPlatformCount % 13 == 0 && spawnPlatformCount > 6 && !JumpPower)
            {
                spawnPlatformCount++;
                TileposZTarget += 5f + Distance;
            }
            else
            {
                spawnPlatformCount++;
                TileposZTarget += 5f;
            }



            if (JumpPower)
            {
                t.transform.position = new Vector3(0, 88.95f, TileposZTarget);
            }
            else
            {
                t.transform.position = new Vector3(posX, 88.95f, TileposZTarget);
            }


            //PlateformChange
            //t.transform.position = new Vector3(posX, 0f, spawnPlatformCount * 5f);
            //t.transform.position = new Vector3(posX, 86.9f, spawnPlatformCount * 5f);
            //t.transform.position = new Vector3(posX, 90f, spawnPlatformCount * 5f); 88.95
            //t.transform.position = new Vector3(posX, 88.95f, (spawnPlatformCount * 5f) + ExtraTileDistance); // 5f convert to distance variable for use in ball and tile distance
            //t.transform.position = new Vector3(posX, 88.95f, TileposZTarget); // 5f convert to distance variable for use in ball and tile distance
            //t.transform.position = new Vector3(posX, 90.95f, spawnPlatformCount * 5f); 


        }

        private IEnumerator HideBoostText()
        {
            yield return new WaitForSeconds(3.0f);
            BoostModeText.gameObject.SetActive(false);
            LvlUpParticals.gameObject.SetActive(false);
        }

        void GameOver()
        {


            AdCOuntDownPanel.SetActive(false);
            MinusKeyPanel.SetActive(false);
            PreOutScreenCanvas.SetActive(false);

            //if (isGameOver)
            //return;

            isGameOver = true;


          


            PlaySoundFall();

            Util.SetDiamond(Util.GetDiamond() + diamond);
            Util.SetKey(Util.GetKey() + key);

            print("key = " + Util.GetKey());

            Util.SetLastScore(point);

            //GPGDemo.Instance.OnAddScoreToLeaderBorad(point,GPGSIds.LeadeboardId[PlayerPrefs.GetInt("LevelIndex", 1)-1]);

            GameServices.ReportScore(point, EM_GameServicesConstants.Leadeboard[PlayerPrefs.GetInt("LevelIndex", 1) - 1], (bool success) =>
            {
                if (success)
                {
                    Debug.Log("Update Score Success  $>>>>> " + point);
                    //NativeUI.ShowToast("Update Score Success");
                }
                else
                {
                    Debug.Log("Update Score Fail  $>>>>> " + point);
                    //NativeUI.ShowToast("Update Score Fail");
                }
            });

            //#if AADOTWEEN
            DOTween.KillAll(false);
            playerParent.DOKill(false);
            playerSphere.DOKill(false);
            shadow.DOKill(false);
            //#endif

            shadow.gameObject.SetActive(false);

            playerSphere.DOLocalMoveY(-10, 1).OnComplete(() =>
            {
                //ShowAds();
                int diamonddd = UnityEngine.Random.Range(point / 2, point);
                int keyyy;// = UnityEngine.Random.Range(point / 100, (point * 2) / 100);

                if (50 < point)
                {
                    GiftScreenCanvas.SetActive(true);

                    if (250 < point)
                    {
                        keyyy = UnityEngine.Random.Range(5, 8);
                        GIftManagerForLevel.giftManagerForLevelRef.ShowGift(2, diamonddd, keyyy);
                    }
                    else if (150 < point)
                    {
                        keyyy = UnityEngine.Random.Range(2, 5);
                        GIftManagerForLevel.giftManagerForLevelRef.ShowGift(3, diamonddd, keyyy);
                    }
                    else if (50 < point)
                    {
                        keyyy = UnityEngine.Random.Range(1, 2);
                        GIftManagerForLevel.giftManagerForLevelRef.ShowGift(1, diamonddd);
                    }
                }
                else
                {
                    SetOutScreenData();
                }


            });
        }

        //private void abc(bool obj)
        //{
        //    //NativeUI.ShowToast("Report Score : "+obj);
        //}

        public void SetOutScreenData()
        {
            GiftScreenCanvas.SetActive(false);
            print("SetActiveOutScren>>>>>");
            OutScreenCan.SetActive(true);

            int GameBaseImageIndex = Util.GetBaseImage(point);

            //print("GameBaseImageIndex"+ GameBaseImageIndex);

            //if (GameBaseImageIndex == 0)
            //{
            //    AddGiftPanel.SetActive(false);
            //}
            //else if (GameBaseImageIndex < 4)
            //{
            //    AddGiftPanel.SetActive(true);
            //    Util.PlusGift(GameBaseImageIndex);
            //}

            if (GameBaseImageIndex == 3)
            {
                BaseImage1.gameObject.SetActive(true);
                BaseImage2.gameObject.SetActive(true);
                BaseImage3.gameObject.SetActive(true);
            }
            else if (GameBaseImageIndex == 2)
            {
                BaseImage1.gameObject.SetActive(true);
                BaseImage2.gameObject.SetActive(true);
            }
            else if (GameBaseImageIndex == 1)
            {
                BaseImage1.gameObject.SetActive(true);
            }
            else
            {
                BaseImage1.gameObject.SetActive(false);
                BaseImage2.gameObject.SetActive(false);
                BaseImage3.gameObject.SetActive(false);
            }

            KeyText.text = Util.GetKey().ToString();
            diamondText.text = Util.GetDiamond().ToString();
            

            TotalDiamondText.text = Util.GetDiamond().ToString();
            TotalKeyText.text = Util.GetKey().ToString();
            LevelNameText.text = Util.GetLevelName();
            ScoreText.text = point.ToString();

            // BaseImage.sprite = Util.GetBaseImage(point);
            //BaseImage.sprite = Resources.Load<Sprite>(GameBaseImageIndex + "star");
            BaseImage.sprite = Resources.Load<Sprite>("0star");

            HighestScoreText.text = Util.GetBestScore().ToString();
            GotDiamondInGameText.text = "+" + diamond.ToString();
            GotKeyInGameText.text = "+" + key.ToString();

            StartCoroutine(CaptureScreenshot());
        }

        public void ReturnFromAdAddExtraX4Diamonds()
        {
            Util.SetDiamond(Util.GetDiamond() + diamond);
            Util.SetKey(Util.GetKey() + key);

            diamond *= 4;
            key *= 2;

            SetOutScreenData();
            X4Button.gameObject.SetActive(false);
        }

        // Coroutine that captures a screenshot and generates a Texture2D object of it  
        IEnumerator CaptureScreenshot()
        {
            // Wait until the end of frame  
            yield return new WaitForEndOfFrame();

            // Create a Texture2D object of the screenshot using the CaptureScreenshot() method
            SStexture = Sharing.CaptureScreenshot();
            //NativeUI.ShowToast("CaptureScreenshot");
        }

        public void ClickToHome()
        {
            PlaySoundJump();
            print("HomeBtnClick");
            SceneManager.LoadSceneAsync(0, LoadSceneMode.Single);
        }

        public void ClickToRestart()
        {
            PlaySoundJump();
            print("RestartBtnClick");
            SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);

            //Relive Achievement
            PlayerPrefs.SetInt("ReTryCounter", (PlayerPrefs.GetInt("ReTryCounter", 0) + 1));
            PlayerPrefs.Save();

            if (PlayerPrefs.GetInt("ReTryCounter", 0) >= 100)
            {
                GameServices.UnlockAchievement(EM_GameServicesConstants.Achievement_AchivementRetry100);
            }
            else if (PlayerPrefs.GetInt("ReTryCounter", 0) >= 50)
            {
                GameServices.UnlockAchievement(EM_GameServicesConstants.Achievement_AchivementRetry50);
            }
            else if (PlayerPrefs.GetInt("ReTryCounter", 0) >= 10)
            {
                GameServices.UnlockAchievement(EM_GameServicesConstants.Achievement_AchivementRetry10);
            }
        }

        public void ClickToShare()
        {
            PlaySoundJump();
            print("ShareBtnClick");
            // NativeUI.ShowToast("ShareBtnClick");
            // Share a Texture2D
            // sampleTexture is a Texture2D object captured some time before
            // This method saves the texture as a PNG image named "screenshot.png" in persistentDataPath,
            // then shares it with a sample message and an empty subject
            //Sharing.ShareTexture2D(ScreenShotImage, "HexaOnPlay", "I challenge you to beat my high score in Hexa On Play  " + "https://play.google.com/store/apps/details?id=com.anni.hexonPlay");
#if UNITY_ANDROID
            Sharing.ShareTexture2D(SStexture, "Piano Tiles Jump", "I challenge you to beat my high score in Piano Tiles Jump " + "https://play.google.com/store/apps/details?id=com.AnniPower.PianoTileJump");
#elif UNITY_IOS
            Sharing.ShareTexture2D(SStexture, "Piano Tiles Jump", "I challenge you to beat my high score in Piano Tiles Jump " + "https://apps.apple.com/us/app/piano-tiles-hop-piano-game/id1483568208");
#endif

        }

        public void OnGiftClick()
        {
            PlaySoundJump();
            print("HomeBtnClick");
            PlayerPrefs.SetInt("OutGiftClick", 1);
            PlayerPrefs.Save();
            SceneManager.LoadSceneAsync(0, LoadSceneMode.Single);
        }



        void ShowAds()
        {
            //int count = PlayerPrefs.GetInt("GAMEOVER_COUNT", 0);
            //count++;
            //PlayerPrefs.SetInt("GAMEOVER_COUNT", count);
            //PlayerPrefs.Save();
            //if (count > numberOfPlayToShowInterstitial)


            if (point > 15)
            {
                PlayerPrefs.SetInt("GAMEOVER_COUNT", 0);
                AdsManagerForLevel.Instance.ShowInterstialAds(0);
            }

        }
    }
}