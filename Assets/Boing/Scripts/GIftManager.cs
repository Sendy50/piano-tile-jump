﻿using AppAdvisory.Boing;
using EasyMobile;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GIftManager : MonoBehaviour
{
    public GameObject GrabedDiamondAndKeyPanel;
    public GameObject AllGiftPanel;
    public GameObject NoGiftPanel;
    public GameObject KeyAnimImage;
    public GameObject DiamondAndKeyImage;
    public GameObject DiamondAnimImage;
    public GameObject OpenBoxAnimImage;
    public GameObject OpenBoxImage;
    public TMP_Text DiamondText;
    public TMP_Text KeyText;
    public Text BtnText;
    public Text ExtraBtnText;
    public Button CloseBtn;
    public Button OpenBtn;
    public Button NoThanksBtn;
    public Button ExtraAdBtn;
    private bool openBtn = true;
    private AnimationClip anim;
    private float currCountdownValue;
    private float currCountdownValue1;
    private int GiftDiamond;
    private int GiftKey;
    private int GiftType;
    private bool MinusGift = false;
    public static GIftManager giftManagerRef;
    private Coroutine AddDiamondCoroutine;
    private Coroutine AddKeyCoroutine;

    private void Awake()
    {
        giftManagerRef = this;
    }


    private void Update()
    {
        //if (BtnText.text == "Collect")
        //{
        //    OpenBtn.transform.GetChild(1).gameObject.SetActive(true);
        //    //BtnText.transform.position = new Vector2(35f, -7.625f);
        //    BtnText.GetComponent<RectTransform>().anchoredPosition = new Vector2(60f, -7.625f);
        //}
        //else
        //{
        //    OpenBtn.transform.GetChild(1).gameObject.SetActive(false);
        //    //BtnText.transform.position = new Vector2(0f, -7.625f);
        //    BtnText.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, -7.625f);
        //}
        if (OpenBtn.transform.GetChild(1).gameObject.activeInHierarchy)
            OpenBtn.transform.GetChild(1).gameObject.SetActive(false);

        if (BtnText.GetComponent<RectTransform>().anchoredPosition.x != 0)
            BtnText.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, -7.625f);
       
    }

    // Start is called before the first frame update
    public void Start()
    {
        print("Start method Call");
        //PlayerPrefs.DeleteAll();
        if (PlayerPrefs.GetInt("Reward",0) == 1)
        {
            AllGiftPanel.SetActive(true);
            NoGiftPanel.SetActive(false);
            CloseBtn.gameObject.SetActive(false);
            switch (PlayerPrefs.GetInt("LastRewardDay", 1))
            {
                case 1:
                    GiftType = 1;
                    GiftDiamond = 50;
                    GiftKey = 0;
                    break;
                case 2:
                    GiftType = 1;
                    GiftDiamond = 100;
                    GiftKey = 0;
                    break;
                case 3:
                    GiftType = 1;
                    GiftDiamond = 300;
                    GiftKey = 0;
                    break;
                case 4:
                    GiftType = 3;
                    GiftDiamond = 0;
                    GiftKey = 5;
                    break;
                case 5:
                    GiftType = 3;
                    GiftDiamond = 0;
                    GiftKey = 10;
                    break;
                case 6:
                    GiftType = 3;
                    GiftDiamond = 0;
                    GiftKey = 15;
                    break;
                case 7:
                    GiftType = 2;
                    GiftDiamond = 500;
                    GiftKey = 20;
                    break;
            }      
        }
        else if (PlayerPrefs.GetInt("AvailGift", 0) > 0)
        {
            AllGiftPanel.SetActive(true);
            NoGiftPanel.SetActive(false);
            CloseBtn.gameObject.SetActive(false);

            GiftType = Util.GetGiftType();
            print("GiftType-- : " + GiftType);
            if (GiftType == 1)   
            {
                GiftDiamond = 100;
                GiftKey = 0;

            }
            else if (GiftType == 2)
            {
                GiftDiamond = 50;
                GiftKey = 5;
            }
            else if (GiftType == 3)
            {
                GiftDiamond = 0;
                GiftKey = 7;
            }
            else if (GiftType == 4)
            {
                GiftType = 2;
                MinusGift = true;
                //GiftDiamond = Util.Diamond4GIft;
                //GiftKey = Util.Key4GIft;
                GiftDiamond = PlayerPrefs.GetInt("Diamond4Gift",50);
                GiftKey = PlayerPrefs.GetInt("Key4Gift",2);

            }
        }
        else
        {
            //Any GIFT Not Available
            AllGiftPanel.SetActive(false);
            NoGiftPanel.SetActive(true);
            CloseBtn.gameObject.SetActive(true);
        }
    }

    

    public void BtnClick()
    {

        StartCoroutine(OpenBtnClick());

//        print("Open Btn Click " + openBtn);
//        if (openBtn)
//        {
//            if (Advertising.IsRewardedAdReady())
//            {
//                AdsManager.Instance.ShowRewardAds(77);
//            }
//            else
//            {
//#if UNITY_EDITOR
//                print("Reward isn't Available In Editor");
//#elif UNITY_ANDROID
//            NativeUI.ShowToast("Reward isn't Available");
//#elif UNITY_IPHONE || UNITY_IOS
//            NativeUI.Alert("Failed","Reward isn't Available","ok");
//#endif
//            }
//        }
//        else
//        {
//            StartCoroutine(OpenBtnClick()); 
//        }
       
       // StartCoroutine(OpenBtnClick());
    }


    public void NoThanksBtnClick()
    {
        CloseButton();
    }


    //Return from Ads id = 77
    public IEnumerator OpenBtnClick()
    {
        if (openBtn)
        {
            MainScreenManager.mainScreenManagerRef.PlayButtonClick();
            GrabedDiamondAndKeyPanel.SetActive(true);

            NoThanksBtn.gameObject.SetActive(false);
            BtnText.text = "Collect";
            ExtraBtnText.text = GiftType == 3 ? "Extra x2" : "Extra x4";
            OpenBoxImage.SetActive(false);
            openBtn = false;
            OpenBoxAnimImage.SetActive(true);
            DiamondAndKeyImage.SetActive(true);
            DiamondAndKeyImage.GetComponent<Image>().sprite = Resources.Load<Sprite>(GiftType == 1 ? "Button/diamond 1" : GiftType == 2 ? "Button/Mix" : "Button/chavi 1");
            OpenBtn.gameObject.SetActive(false);



            yield return new WaitForSeconds(1f);
            DiamondAnimImage.SetActive(GiftType == 1 || GiftType == 2);
            KeyAnimImage.SetActive(GiftType == 3 || GiftType == 2);
            AddDiamondCoroutine = StartCoroutine(AddDiamond(GiftDiamond,true));
            AddKeyCoroutine = StartCoroutine(AddKey(GiftKey,true));
        }
        else
        {
            print("LAst Event Closed");
            if (PlayerPrefs.GetInt("Reward", 0) == 0)
            {
                Util.MinusGift();
            }
            else if (MinusGift)
            {
                Util.MinusGift();
            }
            else
            {
                PlayerPrefs.SetInt("Reward", 0);
                PlayerPrefs.Save();
                //MainScreenManager.mainScreenManagerRef.OpenDailyReward(false);
                //MainScreenManager.mainScreenManagerRef.OpenDailyReward(true);
            }

            AdsManager.Instance.ShowInterstialAds();

            Util.SetDiamond(Util.GetDiamond() + GiftDiamond);
            Util.SetKey(Util.GetKey() + GiftKey);
            OpenBtn.gameObject.SetActive(false);

            yield return new WaitForSeconds(0.2f);
           
            MainScreenManager.mainScreenManagerRef.CoinDropSound(); 
            MainScreenManager.mainScreenManagerRef.UpdateDiamondKeyText();

            DiamondText.text = "0";
            KeyText.text = "0";
            GrabedDiamondAndKeyPanel.SetActive(false);
            OpenBoxAnimImage.SetActive(false);
            DiamondAndKeyImage.SetActive(false);
            DiamondAnimImage.SetActive(false);
            KeyAnimImage.SetActive(false);
            OpenBoxImage.SetActive(true);
            OpenBtn.gameObject.SetActive(true);
            ExtraAdBtn.gameObject.SetActive(false);
            BtnText.text = "Open";
            NoThanksBtn.gameObject.SetActive(true);
            openBtn = true;
            MainScreenManager.mainScreenManagerRef.OpenGift(false);
        }
    }


    public IEnumerator ReturnToExtraRewardAdd()
    {
        OpenBoxAnimImage.SetActive(false);
        DiamondAndKeyImage.SetActive(false);
        ExtraAdBtn.gameObject.SetActive(false);
        print("call Extra x4");
        GrabedDiamondAndKeyPanel.SetActive(true);
        NoThanksBtn.gameObject.SetActive(false);
        BtnText.text = "Collect";
        OpenBoxImage.SetActive(false);
        openBtn = false;
        OpenBoxAnimImage.SetActive(true);
        DiamondAndKeyImage.SetActive(true);
        DiamondAndKeyImage.GetComponent<Image>().sprite = Resources.Load<Sprite>(GiftType == 1 ? "Button/diamond 1" : GiftType == 2 ? "Button/Mix" : "Button/chavi 1");
        OpenBtn.gameObject.SetActive(false);


        yield return new WaitForSeconds(1f);
        DiamondAnimImage.SetActive(GiftType == 1 || GiftType == 2);
        KeyAnimImage.SetActive(GiftType == 3 || GiftType == 2);
        GiftDiamond *= 4;
        GiftKey *= 2;
        AddDiamondCoroutine = StartCoroutine(AddDiamond(GiftDiamond, false));
        AddKeyCoroutine = StartCoroutine(AddKey(GiftKey, false));
    }



    public IEnumerator AddDiamond(float countdownValue, bool extra)
    {
        print("countdownValue " + countdownValue);
        currCountdownValue = 0;
        while (currCountdownValue < countdownValue)
        {           
            //Debug.Log("Countdown: " + currCountdownValue);
            yield return new WaitForSeconds(0.5f);
            currCountdownValue += countdownValue/5;
            DiamondText.text = currCountdownValue.ToString("0");
            print("currCountdownValue " + currCountdownValue);
            MainScreenManager.mainScreenManagerRef.CoinDropSound();


        }

        if (currCountdownValue >= countdownValue && GiftType == 2 || GiftType == 1)
        {
            DiamondAnimImage.SetActive(false);
            KeyAnimImage.SetActive(false);         
            OpenBtn.gameObject.SetActive(true);
            ExtraAdBtn.gameObject.SetActive(extra);
            //StopCoroutine(AddDiamondCoroutine);
            StopAllCoroutines();
        }
    }

    public IEnumerator AddKey(float countdownValue , bool extra)
    {
        currCountdownValue1 = 0;
        while (currCountdownValue1 < countdownValue)
        {

            //Debug.Log("Countdown: " + currCountdownValue1);
            yield return new WaitForSeconds(extra == true ? 0.01f : 0.01f);
            currCountdownValue1 += countdownValue/5;
            KeyText.text = currCountdownValue1.ToString("0");
            MainScreenManager.mainScreenManagerRef.CoinDropSound();
        }

        if (currCountdownValue1 >= countdownValue && GiftType == 3)
        {
            DiamondAnimImage.SetActive(false);
            KeyAnimImage.SetActive(false);
            StopCoroutine(AddKeyCoroutine);
            OpenBtn.gameObject.SetActive(true);
            ExtraAdBtn.gameObject.SetActive(extra);
        }
    }

    public void AfterAdAddGiftButton()
    {
        //call this method from AdManager
        Util.PlusGift(1);  //(int)Util.GetRandomNumber(1,3)
        AllGiftPanel.SetActive(true);
        NoGiftPanel.SetActive(false);
        CloseBtn.gameObject.SetActive(false);
        MainScreenManager.mainScreenManagerRef.OpenGift(false);
    }

    public void CloseButton()
    {
        print("close Button");
        DiamondText.text = "0";
        KeyText.text = "0";
        GrabedDiamondAndKeyPanel.SetActive(false);
        OpenBoxAnimImage.SetActive(false);
        DiamondAndKeyImage.SetActive(false);
        DiamondAnimImage.SetActive(false);
        KeyAnimImage.SetActive(false);
        OpenBoxImage.SetActive(true);
        OpenBtn.gameObject.SetActive(true);
        ExtraAdBtn.gameObject.SetActive(false);
        BtnText.text = "Open";
        NoThanksBtn.gameObject.SetActive(true);
        openBtn = true;
        MainScreenManager.mainScreenManagerRef.OpenGift(false);
    }
}
