﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiftModel
{
    public string GiftBoxIcon; // box sprite name in text
    public int Type; // 1,2,3,4 \\ diamond,key,mix,spin,etc
    public int Value; //200,400,600,1000 etc

    public GiftModel(string GiftBoxIcon, int Type, int Value)
    {
        this.GiftBoxIcon = GiftBoxIcon;
        this.Type = Type;
        this.Value = Value;
    }

}
