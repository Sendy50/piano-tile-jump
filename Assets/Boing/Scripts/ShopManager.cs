﻿using AppAdvisory.Boing;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyMobile;
using TMPro;
using UnityEngine.UI;

public class ShopManager : MonoBehaviour
{
    public int[] PriceList;
    public int[] QuantityList;
    public GameObject PreBuyPanel;
    public TMP_Text ShopItemTitle;
    public Image ShopImage;
    private int TotalDiamond;
    private int TotalKeys;
    private int NeddedDiamond;
    private int index;
    public static ShopManager ShopMangerRef;

    // Start is called before the first frame update
    private void Awake()
    {
        ShopMangerRef = this;
    }


    public void BuyKeyByDiamond(int Index)
    {
        MainScreenManager.mainScreenManagerRef.PlayButtonClick();
        TotalDiamond = Util.GetDiamond();
        TotalKeys = Util.GetKey();
        NeddedDiamond = PriceList[Index];
        if (TotalDiamond >= NeddedDiamond)
        {
            index = Index;
            ShopItemTitle.text = "Add +" + QuantityList[Index] + " Key use for Relive";
            ShopImage.sprite = Resources.Load<Sprite>("Button/chavi 1");
            PreBuyPanel.SetActive(true);
        }
        else
        {

#if UNITY_EDITOR
            print("Insuffience Diamonds");
#elif UNITY_ANDROID
           
        NativeUI.AlertPopup alert = NativeUI.ShowTwoButtonAlert("INSUFFICIENT DIAMOND", "You want to Buy Key ?", "Cancel", "Buy");

        // Subscribe to the event
        if (alert != null)
        {
            alert.OnComplete += OnAlertCompleteHandler;
        }
#elif UNITY_IPHONE || UNITY_IOS
       
        NativeUI.AlertPopup alert = NativeUI.ShowTwoButtonAlert("INSUFFICIENT DIAMOND", "You want to Buy Key ?", "Cancel", "Buy");

        // Subscribe to the event
        if (alert != null)
        {
            alert.OnComplete += OnAlertCompleteHandler;
        }
            
#endif
            //NativeUI.ShowToast("Insuffience Diamonds");
            print("Insuffience Diamonds");
        }
    }

    public void BuyKeyFinalByDiamond()
    {
        MainScreenManager.mainScreenManagerRef.PlayButtonClick();
        PlayerPrefs.SetInt("_DIAMOND", (TotalDiamond - PriceList[index]));
        PlayerPrefs.SetInt("_KEY", (TotalKeys + QuantityList[index]));
        PlayerPrefs.Save();
        MainScreenManager.mainScreenManagerRef.UpdateDiamondKeyText();
        //NativeUI.ShowToast("+" + QuantityList[index] + " Key Added Successfull");


#if UNITY_EDITOR
        print("+" + QuantityList[index] + " Key Added Successfull");
#elif UNITY_ANDROID
            NativeUI.ShowToast("+" + QuantityList[index] + " Key Added Successfull");
#elif UNITY_IPHONE || UNITY_IOS
            NativeUI.Alert("Key Added!","+" + QuantityList[index] + " Key Added Successfull","ok");
#endif
        print("+" + QuantityList[index] + " Key Added Successfull");
        PreBuyPanel.SetActive(false);
    }

    public void ReturnFromRewardAdX2Key()
    {
        //Show Interstial Ad And Than call it's from AdManager
        PlayerPrefs.SetInt("_DIAMOND", (TotalDiamond - PriceList[index]));
        PlayerPrefs.SetInt("_KEY", (TotalKeys + (QuantityList[index] * 2)));
        PlayerPrefs.Save();
        MainScreenManager.mainScreenManagerRef.UpdateDiamondKeyText();
        //NativeUI.ShowToast("+" + (QuantityList[index] * 2) + " Key Added Successfull");


#if UNITY_EDITOR
        print("+" + (QuantityList[index] * 2) + " Key Added Successfull");
#elif UNITY_ANDROID
            NativeUI.ShowToast("+" + (QuantityList[index] * 2) + " Key Added Successfull");
#elif UNITY_IPHONE || UNITY_IOS
            NativeUI.Alert("Key Added!","+" + (QuantityList[index] * 2) + " Key Added Successfull","ok");
#endif

        //print("+" + (QuantityList[index] * 2) + " Key Added Successfull");
        PreBuyPanel.SetActive(false);
    }


    // The event handler
    public void OnAlertCompleteHandler(int buttonIndex)
    {
        switch (buttonIndex)
        {
            case 0:
                // Button 1 was clicked
                break;
            case 1:
                // Button 2 was clicked
                if ((NeddedDiamond - TotalDiamond) < 1000)
                {
                    IAPManager.IAPManagerRef.PurchasePack(0);
                }
                else
                {
                    IAPManager.IAPManagerRef.PurchasePack(1);
                }
                break;
            default:
                break;
        }
    }

    public void closePreBuyPanel()
    {
        PreBuyPanel.SetActive(false);
    }

   
}
