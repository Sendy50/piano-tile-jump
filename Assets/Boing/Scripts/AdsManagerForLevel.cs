﻿//using admob;
using AppAdvisory.Boing;
using EasyMobile;
using GoogleMobileAds.Api.Mediation.UnityAds;
//using GoogleMobileAdsMediationTestSuite.Api;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdsManagerForLevel : MonoBehaviour, IUnityAdsListener
{
    public static AdsManagerForLevel Instance { get; private set; }
    private int Ad_idd;
    //Admob ad;

    //string gameId = "3286005";


#if UNITY_IOS
    private string gameId = "3286004";
    private string nativeBannerID = "ca-app-pub-3793952003849557/3798485841";
     public string appID = "ca-app-pub-3793952003849557~5723763493";
#elif UNITY_ANDROID
    private string gameId = "3286005";
    private string nativeBannerID = "ca-app-pub-3793952003849557/9816404752";
    public string appID = "ca-app-pub-3793952003849557~7524269636";
#endif

    string myPlacementId = "rewardedVideo";
    string InterPlacementId = "video";
    bool testMode = true;
    private string placementIdd;
    private int InterId;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        if (!RuntimeManager.IsInitialized())
            RuntimeManager.Init();
    }


    //public void InitADMOB()
    //{
    //    AdProperties adProperties = new AdProperties();
    //    adProperties.isTesting = false;

    //    ad = Admob.Instance();
    //    ad.bannerEventHandler += onBannerEvent;
    //    ad.nativeBannerEventHandler += onNativeBannerEvent;
    //    ad.initSDK(appID, adProperties);
    //}

    //Start is called before the first frame update
    void Start()
    {
        UnityAds.SetGDPRConsentMetaData(true);
        Advertisement.Initialize(gameId, testMode);
        Advertisement.AddListener(this);

        // MediationTestSuite.OnMediationTestSuiteDismissed += this.HandleMediationTestSuiteDismissed;
    }

    //private void HandleMediationTestSuiteDismissed(object sender, EventArgs e)
    //{
    //    NativeUI.ShowToast("HandleMediationTestSuiteDismissed event received");
    //}

    //private void HandleMediationTestSuiteDismissed(object sender, EventArgs e)
    //{
    //    NativeUI.ShowToast("HandleMediationTestSuiteDismissed event received");
    //}

    //void IUnityAdsListener.OnUnityAdsDidError(string message)
    //{
    //    print("AdManahgerForLevel AdsShow OnUnityAdsDidError");
    //}

    //void IUnityAdsListener.OnUnityAdsDidStart(string placementId)
    //{
    //    print("AdManahgerForLevel AdsShow OnUnityAdsDidStart");
    //}

    //void HandleShowResult2(ShowResult showResult)
    //{
    //    if (showResult == ShowResult.Finished)
    //    {
    //        print("AdManahgerForLevel AdsShow Finished");

    //        //Paste That Code in Close Or Fail Ad Event       
    //        if (Ad_idd == 4)
    //        {
    //            GameManager.gameManagerRef.ReturnFromAdAddExtraX4Diamonds();
    //        }
    //        else if (Ad_idd == 5)
    //        {
    //            GameManager.gameManagerRef.ReturnFromAdReLiveByCountDown();
    //        }
    //        else if (Ad_idd == 6)
    //        {
    //            print("AdManahgerForLevel finish gift x4");
    //            StartCoroutine(GIftManagerForLevel.giftManagerForLevelRef.ReturnToExtraRewardAdd());
    //        }
    //        //End
    //    }
    //    else if (showResult == ShowResult.Skipped)
    //    {
    //        print("AdManahgerForLevel AdsShow Skipped");
    //    }
    //    else if (showResult == ShowResult.Failed)
    //    {
    //        print("AdManahgerForLevel AdsShow Failed");
    //        NativeUI.ShowToast("Reward is not Ready", true);
    //    }
    //}

    //void IUnityAdsListener.OnUnityAdsReady(string placementId)
    //{
    //    print("AdManahgerForLevel AdsShow OnUnityAdsReady " + placementId);
    //    placementIdd = placementId;
    //}

    // [System.Obsolete]


    //public void ShowNativeCenter()
    //{
    //    print("Show NAtive Ads ####################### ");
    //    Admob.Instance().showNativeBannerRelative(nativeBannerID, new AdSize(320, 280), AdPosition.MIDDLE_CENTER);
    //}


    //public void HideNativeAdsCenter()
    //{
    //    print("Hide NAtive Ads ####################### ");
    //    Admob.Instance().removeNativeBanner();
    //}


    public void ShowBannerAds()
    {
        if (PlayerPrefs.GetInt("RemoveAds", 0) == 0)
        {
            Advertising.ShowBannerAd(BannerAdPosition.Bottom);
        }
    }

    public void HideBannerAds()
    {
        if (PlayerPrefs.GetInt("RemoveAds", 0) == 0)
        {
            Advertising.DestroyBannerAd();
        }
    }



    public void ShowInterstialAds(int interId)
    {
        InterId = interId;

        if (PlayerPrefs.GetInt("RemoveAds", 0) == 0)
        {
            if (Advertising.IsInterstitialAdReady())
            {
                Advertising.ShowInterstitialAd();
            }
            else
            {
                Advertisement.Show(InterPlacementId);
                print("Interstitial ads of unity");


            }
        }
        else
        {
            if (InterId == 0)
            {
                GameManager.gameManagerRef.ReLiveCheck();
            }
            print("Interstitial ads not Show Because Remove Ads Purchased ...");
        }
    }



    private void InterstitialAdCompletedHandler(InterstitialAdNetwork arg1, AdPlacement arg2)
    {
        if (InterId == 0)
        {
            GameManager.gameManagerRef.ReLiveCheck();
        }
        else
        {

        }
    }



    public void ShowRewardAds(int id)
    {
        // Check if rewarded ad is ready
        bool isReady = Advertising.IsRewardedAdReady();

        // Show it if it's ready
        print("Is Ready : " + isReady);
        print("Ad_Id ???? " + id);
        Ad_idd = id;

        GameManager.gameManagerRef.PlaySoundJump();


        if (isReady)
        {
            Advertising.ShowRewardedAd();
        }
        else
        {
#if UNITY_EDITOR
            print("Reward isn't Available In Editor");
#elif UNITY_ANDROID
            NativeUI.ShowToast("Reward isn't Available");
#elif UNITY_IPHONE || UNITY_IOS
            NativeUI.Alert("Failed","Reward isn't Available","ok");
#endif
        }
    }


    void OnEnable()
    {
        Advertising.RewardedAdCompleted += RewardedAdCompletedHandler;
        Advertising.RewardedAdSkipped += RewardedAdSkippedHandler;
        Advertising.InterstitialAdCompleted += InterstitialAdCompletedHandler;
    }



    // Unsubscribe events
    void OnDisable()
    {
        Advertising.RewardedAdCompleted -= RewardedAdCompletedHandler;
        Advertising.RewardedAdSkipped -= RewardedAdSkippedHandler;
        Advertising.InterstitialAdCompleted -= InterstitialAdCompletedHandler;
    }


    // Event handler called when a rewarded ad has completed
    void RewardedAdCompletedHandler(RewardedAdNetwork network, AdLocation location)
    {
        Debug.Log("Rewarded ad has completed. The user should be rewarded now.");

        //Paste That Code in Close Or Fail Ad Event       
        if (Ad_idd == 4)
        {
            GameManager.gameManagerRef.ReturnFromAdAddExtraX4Diamonds();
        }
        else if (Ad_idd == 5)
        {
            GameManager.gameManagerRef.ReturnFromAdReLiveByCountDown();
        }
        else if (Ad_idd == 6)
        {
            print("AdManahgerForLevel finish gift x4");
            StartCoroutine(GIftManagerForLevel.giftManagerForLevelRef.ReturnToExtraRewardAdd());
        }
        else if (Ad_idd == 77)
        {
            StartCoroutine(GIftManagerForLevel.giftManagerForLevelRef.OpenBtnClick());
        }
        //End
    }

    // Event handler called when a rewarded ad has been skipped
    void RewardedAdSkippedHandler(RewardedAdNetwork network, AdLocation location)
    {

#if UNITY_EDITOR
        print("Reward isn't Available In Editor");
#elif UNITY_ANDROID
            NativeUI.ShowToast("Rewarded was Failed");
#elif UNITY_IPHONE || UNITY_IOS
            NativeUI.Alert("Failed","Rewarded was Failed","ok");
#endif

        if (Ad_idd == 5)
        {
            GameManager.gameManagerRef.NoThanksBtnClick();
        }
    }


    public void OnUnityAdsReady(string placementId)
    {
        throw new NotImplementedException();
    }

    public void OnUnityAdsDidError(string message)
    {
        throw new NotImplementedException();
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        throw new NotImplementedException();
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        if (showResult == ShowResult.Finished)
        {

            if (placementId == myPlacementId)
            {
                Debug.Log("Rewarded ad has completed. The user should be rewarded now.");

                //Paste That Code in Close Or Fail Ad Event       
                if (Ad_idd == 4)
                {
                    GameManager.gameManagerRef.ReturnFromAdAddExtraX4Diamonds();
                }
                else if (Ad_idd == 5)
                {
                    GameManager.gameManagerRef.ReturnFromAdReLiveByCountDown();
                }
                else if (Ad_idd == 6)
                {
                    print("AdManahgerForLevel finish gift x4");
                    StartCoroutine(GIftManagerForLevel.giftManagerForLevelRef.ReturnToExtraRewardAdd());
                }
                else if (Ad_idd == 77)
                {
                    StartCoroutine(GIftManagerForLevel.giftManagerForLevelRef.OpenBtnClick());
                }
            }
            else
            {
                if (InterId == 0)
                {
                    GameManager.gameManagerRef.ReLiveCheck();
                }
                else
                {

                }
            }
        }
        else if (showResult == ShowResult.Skipped)
        {
            if (placementId == myPlacementId)
            {
#if UNITY_EDITOR
                print("Reward isn't Available In Editor");
#elif UNITY_ANDROID
            NativeUI.ShowToast("Rewarded was Failed");
#elif UNITY_IPHONE || UNITY_IOS
            NativeUI.Alert("Failed","Rewarded was Failed","ok");
#endif

                if (Ad_idd == 5)
                {
                    GameManager.gameManagerRef.NoThanksBtnClick();
                }
            }
        }
        else if (showResult == ShowResult.Failed)
        {
            if (placementId == myPlacementId)
            {
#if UNITY_EDITOR
                print("Reward isn't Available In Editor");
#elif UNITY_ANDROID
            NativeUI.ShowToast("Rewarded was Failed");
#elif UNITY_IPHONE || UNITY_IOS
            NativeUI.Alert("Failed","Rewarded was Failed","ok");
#endif
            }
            else
            {
                if (InterId == 0)
                {
                    GameManager.gameManagerRef.ReLiveCheck();
                }
                else
                {

                }
                print("Interstitial ads not Loaded...");
            }
        }
    }
}
