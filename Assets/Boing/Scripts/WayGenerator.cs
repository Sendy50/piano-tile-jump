﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayGenerator : MonoBehaviour
{

    public GameObject WayPrefab;
    private float LastPointLoc;
   // private float count;
    private GameObject go;
    public static WayGenerator wayGeneratorRef;
    public int wayCount = 0;

    // Start is called before the first frame update
    void Start()
    {
        wayGeneratorRef = this;
        for (int i = 0; i < 5; i++)
        {
            SpawnWay();
        } 
    }


    public void SpawnWay()
    {
        // go =  Instantiate(WayPrefab, new Vector3(0f,88f, count),Quaternion.Euler(90f,0f,0f)); 
        if (go != null)
        {
           // go = Instantiate(WayPrefab, new Vector3(0f, 88f, go.transform.position.z + 40.5f), Quaternion.Euler(90f, 0f, 0f));
            go = Instantiate(WayPrefab, new Vector3(0f, 88f, go.transform.position.z + 81f), Quaternion.Euler(90f, 0f, 0f));
            wayCount++;
            //print("SpawnWay " + wayCount);
            
        }
        else
        {
            go = Instantiate(WayPrefab, new Vector3(0f, 88f, 0.0f), Quaternion.Euler(90f, 0f, 0f));
            wayCount++;
            ///print("SpawnWay " + wayCount);
           
            //count += 40.5f;
            //print("Spawn CountLoc z = " + go.transform.position.z);
        }
       
    }

}
