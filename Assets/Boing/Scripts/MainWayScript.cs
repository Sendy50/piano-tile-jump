﻿using AppAdvisory.Boing;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainWayScript : MonoBehaviour
{
    private GameManager gameManager;

    void Awake()
    {
        this.gameManager = FindObjectOfType<GameManager>();
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(CoroutDestroyIfNotVisible());
    }

    IEnumerator CoroutDestroyIfNotVisible()
    {
        while (true)
        {
            if (gameManager.camGame.transform.position.z - transform.position.z > 15)
            {
                print("Destroy way >>.........<< " + WayGenerator.wayGeneratorRef.wayCount);
                Destroy(this.gameObject);               
                break;
            }

            yield return new WaitForSeconds(0.3f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //print("Trigger Out" + other.gameObject.tag);
        //if (other.gameObject.tag == "Player")
        //{
            print("Trigger In");
            WayGenerator.wayGeneratorRef.SpawnWay();
       // }
        
    }
}
