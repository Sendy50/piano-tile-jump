﻿
using UnityEngine;

namespace AppAdvisory.Boing
{
    public class SmoothCamera2D : MonoBehaviour
    {

        float speed = 5f;
        private Vector3 velocity = Vector3.one * 1;
        public Transform target;
        public float y;
        public float z;

        public GameObject BGSprite;
      

        Camera cam;

        void Awake()
        {
            cam = GetComponent<Camera>();
            y = transform.position.y;
            z = transform.position.z;
        }

      

        // Update is called once per frame
        void Update()
        {
            if (target)
            {
                Vector3 point = cam.WorldToViewportPoint(target.position);
                Vector3 delta = target.position - cam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z)); //(new Vector3(0.5, 0.5, point.z));

                Vector3 destination = transform.position + delta;
                //Vector3 pos = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);
                Vector3 pos = Vector3.Lerp(transform.position, destination, speed * Time.deltaTime);

                float posX = pos.x;

                if (posX < -1f)
                    posX = -1f;

                if (posX < +1f)
                    posX = +1f;

                transform.position = new Vector3(pos.x, y, target.position.z + z );

                print("Cam Position : " + transform.position);
              
                if (Util.GetLevelIndex() == 2)
                {
                    BGSprite.gameObject.transform.position = new Vector3(pos.x, y + 19f, target.position.z + z + 92.47f);
                }  
                else if (Util.GetLevelIndex() == 3)
                {
                    BGSprite.gameObject.transform.position = new Vector3(pos.x, y + 13f, target.position.z + z + 92.47f);
                }
                else if (Util.GetLevelIndex() == 4)
                {
                    BGSprite.gameObject.transform.position = new Vector3(pos.x, y + 5f, target.position.z + z + 92.47f);
                }
                else
                {
                    BGSprite.gameObject.transform.position = new Vector3(pos.x, y + 18.0f, target.position.z + z + 92.47f);
                }
            }
        }
    }
}