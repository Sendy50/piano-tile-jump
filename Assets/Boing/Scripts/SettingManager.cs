﻿using System.Collections;
using AppAdvisory.Boing;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using EasyMobile;

public class SettingManager : MonoBehaviour
{
    public GameObject HelpPanel;
    public Image SoundBgImage;
    public Image SoundIconImage;
    public Image MusicBgImage;
    public Image MusicIconImage;

    public TMP_Text SoundOnText;
    public TMP_Text MusicOnText;
    private int Sound;
    private int Music;

    // Start is called before the first frame update
    void Start()
    {

        setSoundUi();
        setMusicUi();

    }

    private void setSoundUi()
    {
        if (PlayerPrefs.GetInt("Sound",1) == 1)
        {
            SoundBgImage.sprite = Resources.Load<Sprite>("shopItemImage/shopItem3");//SettingItemImage/DeActiveShopItem
            SoundIconImage.sprite = Resources.Load<Sprite>("Button/speaker");
            //SoundIconImage.color = new Color(81f / 255f, 80f / 255f, 82f / 255f);
            SoundOnText.text = "On";
        }
        
        else
        {
            SoundBgImage.sprite = Resources.Load<Sprite>("SettingItemImage/DeActiveShopItem");
            SoundIconImage.sprite = Resources.Load<Sprite>("Button/speaker 1");
            //SoundIconImage.color = new Color(221f / 255f, 221f / 255f, 221f / 255f);
            SoundOnText.text = "Off";
        }
    }

    private void setMusicUi()
    {
        if (PlayerPrefs.GetInt("Music", 1) == 1)
        {
            MusicBgImage.sprite = Resources.Load<Sprite>("shopItemImage/shopItem3");//SettingItemImage/DeActiveShopItem
            MusicIconImage.sprite = Resources.Load<Sprite>("Button/music-player");
            //MusicIconImage.color = new Color(81f/255f, 80f / 255f, 82f / 255f); // new Color32(255,255,225,100);
            MusicOnText.text = "On";
        }
        else
        {
            MusicBgImage.sprite = Resources.Load<Sprite>("SettingItemImage/DeActiveShopItem"); 
            //MusicIconImage.color = new Color(221f / 255f, 221f / 255f, 221f / 255f);
            MusicOnText.text = "Off";
        }
    }

   
    public void OpenShop()
    {
        MainScreenManager.mainScreenManagerRef.OpenShop(true);
    }

    public void SoundOnOff()
    {
       
        Sound = PlayerPrefs.GetInt("Sound", 1);
        print("SoundButtonClick : " + Sound);
        PlayerPrefs.SetInt("Sound", (Sound == 1) ? 0 : 1 );
        PlayerPrefs.Save();
        setSoundUi();
    }

    public void MusiconOff()
    {
        Music = PlayerPrefs.GetInt("Music", 1);
        print("MusicButtonClick : " + Music);
        PlayerPrefs.SetInt("Music", (Music == 1) ? 0 : 1);
        PlayerPrefs.Save();
        setMusicUi();      
    }
    
    public void RateUs()
    {
        StoreReview.RequestRating();
    }

    public void Help(bool show)
    {
        HelpPanel.SetActive(show);
    }

    public void PrivatePolicy()
    {

    }
}
