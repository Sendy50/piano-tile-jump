﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using System;
using EasyMobile;

namespace AppAdvisory.Boing
{
    public class MainScreenManager : MonoBehaviour
    {
        public static MainScreenManager mainScreenManagerRef;
        public GameObject ShopCanvas;
        public GameObject GiftCanvas;
        public GameObject SettingCanvas;
        public GameObject FaceBookCanvas;
        public GameObject DailyRewardCanvas;
        public GameObject LevelUnlockCanvas;
        public TMP_Text TotalDiamond;
        public TMP_Text TotalKey;
        public Image RedDotImage;
        public Image ShopRedDotImage;
        public Image CharcterImage;

        public Scrollbar swipeSensibility;
        public GameObject GiftIconImage;
        public GameObject ShopIconImage;

        public GameObject[] LevelUnlock;
        public Image[] LevelBaseImage;
        public Image[] LevelLifeImage;  
        public TMP_Text[] LevelBestScore;
        public Text[] RemainingTime;
        public Sprite[] ChartImage;


        private int GiftDiamond;
        private int GiftKey;
        private DateTime startDate;
        private static DateTime today;
        public int LevelIndex;

        #region sounds
        AudioSource audioSource;
        public AudioClip ButtonClick;
        public AudioClip CoinDropClick;
        public bool newOpenLevel = true;

        public void PlayButtonClick()
        {
            if (PlayerPrefs.GetInt("Sound", 1) == 1)
            {
                audioSource.PlayOneShot(ButtonClick);
            }
        }

        public void CoinDropSound()
        {
            if (PlayerPrefs.GetInt("Sound", 1) == 1)
            {
                audioSource.PlayOneShot(CoinDropClick);
            }
        }
        #endregion


        void Awake()
        {
            mainScreenManagerRef = this;
            audioSource = GetComponent<AudioSource>();
            
        }


        void Start()
        {

            //PlayerPrefs.DeleteAll();
            //Util.SetDiamond(10000);
            if (!GameServices.IsInitialized())
            {
                GameServices.Init();
            }

            newOpenLevel = true;

            ////Util.PlusGift(2);
            //SetStartDate();
            //TodaysDay = (GetDaysPassed() / 7)+2;
            //print("TodaysDay " + TodaysDay);
            //PlayerPrefs.DeleteAll();
            swipeSensibility.value = PlayerPrefs.GetFloat("swipeSensibility", 0.5f);
            swipeSensibility.onValueChanged.AddListener((float val) => ScrollbarCallback(val));
            print("level index "+ (Util.GetLevelIndex()-1));
            CharcterImage.sprite = ChartImage[Util.GetLevelIndex()-1];

            print("YeasterDay Time : " + DateTime.Parse(PlayerPrefs.GetString("LastRewardTime", DateTime.Now.AddDays(-1).ToString())));

            TotalDiamond.text = Util.GetDiamond().ToString(); //PlayerPrefs.GetInt("_DIAMOND", 0).ToString();   //Util.GetDiamond().ToString();
            TotalKey.text = Util.GetKey().ToString(); //PlayerPrefs.GetInt("_KEY", 0).ToString();   //Util.GetKey().ToString();

            RefressLevelUi();

            if (PlayerPrefs.GetInt("OutGiftClick", 0) == 1)
            {
                PlayerPrefs.SetInt("OutGiftClick", 0);
                PlayerPrefs.Save();
                OpenGift(true);
            }

            print("LastRewardTime" + PlayerPrefs.GetString("LastRewardTime"));
            StartCoroutine(OpenDailyReward(Util.RewardIsAvailable()));

            //InvokeRepeating("UnlockLevelChecker", 0, 60);
        }

        void ScrollbarCallback(float value)
        {
            Debug.Log(value);
            PlayerPrefs.SetFloat("swipeSensibility", value);
            PlayerPrefs.Save();
        }

        public void RefressLevelUi()
        {
            //LevelBaseImage[0].sprite = Resources.Load<Sprite>(PlayerPrefs.GetInt("MaxBase1", 0) + "star");
            //LevelBestScore[0].text = PlayerPrefs.GetInt("_BESTSCORE1", 0).ToString();
            // LevelUnlock[1].SetActive(PlayerPrefs.GetInt("2_Unlock", 1) == 0);

            for (int i = 0; i < 13; i++)
            {
                LevelBaseImage[i].sprite = Resources.Load<Sprite>(PlayerPrefs.GetInt("MaxBase"+(i+1), 0) + "star");

                LevelBestScore[i].text = PlayerPrefs.GetInt("_BESTSCORE"+(i+1), 0).ToString();

               // print(i +" _LevelPlayCounter  " + PlayerPrefs.GetInt((i + 1) + "_LevelPlayCounter", 0));

                if(PlayerPrefs.GetInt((i+1) + "_LevelBuy", 0) != 1)
                    LevelLifeImage[i].sprite = Resources.Load<Sprite>("Button/heart"+ PlayerPrefs.GetInt((i+1) + "_LevelPlayCounter", 0));
               


                LevelUnlock[i].SetActive(PlayerPrefs.GetInt((i + 1) + "_Unlock", 1) == 0);

                //if(i != 0)
                //{
                //LevelUnlock[i-1].SetActive(PlayerPrefs.GetInt((i+1)+"_Unlock", 1) == 0);
                // }
            }
         
            //print("RefressLevelUi >>>>>>");
        }

        // Update is called once per frame
        void Update()
        {

#if UNITY_ANDROID
            if (Input.GetKeyUp(KeyCode.Escape) && !SettingCanvas.activeInHierarchy && !DailyRewardCanvas.activeInHierarchy
                 && !LevelUnlockCanvas.activeInHierarchy && !GiftCanvas.activeInHierarchy && !ShopCanvas.activeInHierarchy)
            {
                // Ask if user wants to exit
                NativeUI.AlertPopup alert = NativeUI.ShowTwoButtonAlert("Exit App",
                                                "Do you want to exit?",
                                                "Yes",
                                                "No");

                if (alert != null)
                    alert.OnComplete += delegate (int button)
                    {
                        if (button == 0)
                            Application.Quit();
                    };
            }

#endif

            if (PlayerPrefs.GetInt("AvailGift",0) > 0)
            {
                RedDotImage.gameObject.SetActive(true);
                Animator animtor = GiftIconImage.GetComponent<Animator>();
                animtor.SetBool("opengift", true);
            }
            else
            {
                RedDotImage.gameObject.SetActive(false);
                Animator animtor = GiftIconImage.GetComponent<Animator>();
                animtor.SetBool("opengift", false);
            }

            if (Util.GetDiamond() > 100)
            {
                Animator animtor = ShopIconImage.GetComponent<Animator>();
                animtor.SetBool("openshop", true);
                ShopRedDotImage.gameObject.SetActive(true);
            }
            else
            {
                Animator animtor = ShopIconImage.GetComponent<Animator>();
                animtor.SetBool("openshop", false);
                ShopRedDotImage.gameObject.SetActive(false);
            }

            // print("_Unlock = 1 or lock = 0 =  : " + PlayerPrefs.GetInt(2 + "_Unlock", 0));
            for (int Level = 1; Level < 14; Level++)
            {
               
                if (PlayerPrefs.GetInt(Level + "_Unlock", 1) == 0) //true if Lock
                {
                    //print("Level :" + Level + " Lock Time Remain : " + Util.GetLevelUnlockTime(Level));
                    //print("Level :" + Level + " tempTime " + DateTime.Parse(PlayerPrefs.GetString(LevelIndex + "_LevelLockTime", DateTime.Now.AddDays(-1).ToString())));
                    //print("Level:123   "+ Level  +" " + PlayerPrefs.GetInt( Level + "_Unlock", 0));
                    RemainingTime[Level-1].text = Util.GetLevelUnlockTime(Level);
                }
                else if (PlayerPrefs.GetInt(Level + "_LevelBuy", 0) == 1 || PlayerPrefs.GetInt(Level + "_LevelPlayCounter", 0) >= 3)
                {
                    UnlockLevelChecker(Level);
                }
                else
                {
                    RefressLevelUi();
                }
            }

            //for (int Level = 2; Level < 13; Level++)
            //{

            //    if (PlayerPrefs.GetInt(Level + "_Unlock", 1) == 0) //true if Lock
            //    {
            //        //print("Level :" + Level + " Lock Time Remain : " + Util.GetLevelUnlockTime(Level));
            //        //print("Level :" + Level + " tempTime " + DateTime.Parse(PlayerPrefs.GetString(LevelIndex + "_LevelLockTime", DateTime.Now.AddDays(-1).ToString())));
            //        //print("Level:123   "+ Level  +" " + PlayerPrefs.GetInt( Level + "_Unlock", 0));
            //        RemainingTime[Level - 2].text = Util.GetLevelUnlockTime(Level);
            //    }
            //    else if (PlayerPrefs.GetInt(Level + "_LevelBuy", 0) == 1 || PlayerPrefs.GetInt(Level + "_LevelPlayCounter", 0) >= 3)
            //    {
            //        UnlockLevelChecker(Level);
            //    }
            //    else
            //    {
            //        RefressLevelUi();
            //    }
            //}


        }

        // Subscribe to events in the OnEnable method of a MonoBehavior script
        void OnEnable()
        {
            GameServices.UserLoginSucceeded += OnUserLoginSucceeded;
            GameServices.UserLoginFailed += OnUserLoginFailed;
        }

        // Unsubscribe
        void OnDisable()
        {
            GameServices.UserLoginSucceeded -= OnUserLoginSucceeded;
            GameServices.UserLoginFailed -= OnUserLoginFailed;
        }

        // Event handlers
        void OnUserLoginSucceeded()
        {
            Debug.Log("User logged in successfully. $>>>>>");
            //NativeUI.ShowToast("User logged in successfully.");
        }

        void OnUserLoginFailed()
        {
            Debug.Log("User login failed. $>>>>>");
            //NativeUI.ShowToast("User login failed.");
        }

        public void UnlockLevelChecker(int Level)
        {
            //for (int Level = 2; Level < 8; Level++)
            //{
            //print("UnlockLevelChecker : _LevelBuy " + PlayerPrefs.GetInt(Level + "_LevelBuy", 0));
            if (PlayerPrefs.GetInt(Level + "_LevelBuy", 0) == 1)
            {
                //print("UnlockLevelChecker : " + (DateTime.Now > DateTime.Parse(PlayerPrefs.GetString(Level + "_LevelLockTime"))));
                //print(Level + "_LevelLockTime  : " + PlayerPrefs.GetString(Level + "_LevelLockTime"));
                
                if (DateTime.Now > DateTime.Parse(PlayerPrefs.GetString(Level + "_LevelLockTime")))
                {
                    PlayerPrefs.SetInt(Level + "_Unlock", 0);
                    PlayerPrefs.SetInt(Level + "_LevelBuy", 0);
                    PlayerPrefs.SetString(Level + "_LevelLockTime", DateTime.Now.AddHours(3).ToString());
                    PlayerPrefs.Save();
                    RefressLevelUi();
                    print("UnlockLevelChecker : Lock Level" + DateTime.Now);
                }
                //print(Level + "_LevelLockTime  : " + PlayerPrefs.GetString(Level + "_LevelLockTime"));
            }
            else if (PlayerPrefs.GetInt(Level + "_LevelPlayCounter", 0) >= 3)
            {   
                PlayerPrefs.SetInt(Level + "_Unlock", 0);
                PlayerPrefs.SetInt(Level + "_LevelPlayCounter", 0); 
                PlayerPrefs.SetString(Level + "_LevelLockTime", DateTime.Now.AddHours(3).ToString());
                print(Level + "_LevelLockTime   : " + PlayerPrefs.GetInt(Level + "_LevelPlayCounter", 0));
                PlayerPrefs.Save();
                RefressLevelUi();
                print("UnlockLevelChecker _LevelPlayCounter : Lock Level" + DateTime.Now);      
            }
            else
            {
                print("UnlockLevelChecker else call");
            }
            //}       
        }

        //void SetStartDate()
        //{
        //    if (PlayerPrefs.HasKey("DateInitialized")) //if we have the start date saved, we'll use that
        //        startDate = System.Convert.ToDateTime(PlayerPrefs.GetString("DateInitialized"));
        //    else //otherwise...
        //    {
        //        startDate = System.DateTime.Now; //save the start date ->
        //        PlayerPrefs.SetString("DateInitialized", startDate.ToString());
        //        PlayerPrefs.Save();
        //    }
        // }

        //public int GetDaysPassed()
        //{
        //    today = System.DateTime.Now;

        //    //days between today and start date -->
        //    System.TimeSpan elapsed = today.Subtract(startDate);

        //    double days = elapsed.TotalDays;
        //    print("days" + days + "");
        //    return (int)days;
        //}

        public void OpenLevel(int levelIndex)
        {
            if (newOpenLevel)
            {
                newOpenLevel = false;
                PlayButtonClick();
                PlayerPrefs.SetInt("LevelIndex", levelIndex);
                PlayerPrefs.Save();
                PlayerPrefs.SetInt(levelIndex + "_LevelPlayCounter", PlayerPrefs.GetInt(levelIndex + "_LevelPlayCounter", 0) + 1); 
                PlayerPrefs.Save();
                print("_LevelPlayCounter : " + PlayerPrefs.GetInt(levelIndex + "_LevelPlayCounter", 0));
                print("Level " + levelIndex + "  Playing");
                SceneManager.LoadSceneAsync(levelIndex, LoadSceneMode.Single);
               // print("_LevelPlayCounter : " + PlayerPrefs.GetInt(levelIndex + "_LevelPlayCounter", 0));
            }
        }

        public void Add20Diamond()
        {
            CoinDropSound();
            PlayerPrefs.SetInt("_DIAMOND", (Util.GetDiamond() + 20));
            PlayerPrefs.Save();
            UpdateDiamondKeyText();
        }

        public void Add2Key()
        {
            CoinDropSound();
            PlayerPrefs.SetInt("_KEY", (Util.GetKey() + 2));
            PlayerPrefs.Save();
            UpdateDiamondKeyText();
        }

        public void AddGift(int GiftType)
        {
            //PlayButtonClick();
            print("Gift Index : " + Util.PlusGift(GiftType));
        }

        public void OpenShop(bool show)
        {
            PlayButtonClick();
            ShopCanvas.SetActive(show);
        }

        public IEnumerator OpenDailyReward(bool show)
        {
            if (show)
            {
                yield return new WaitForSeconds(3.0f);
                DailyRewardCanvas.SetActive(true);
            }
            else
            {
                DailyRewardCanvas.SetActive(false);
            }
          
        }

        //IEnumerator WaitForSecondsDo(float secToWait)
        //{
        //    yield return new WaitForSeconds(secToWait);
        //}

        public void OpenSetting(bool show)
        {
            PlayButtonClick();
            SettingCanvas.SetActive(show);
        }
         public void RemoveAdsButtonClick()
        {
            PlayButtonClick();
            IAPManager.IAPManagerRef.PurchasePack(8);
        }

        public void OpenGift(bool show)
        {
            PlayButtonClick();
            if (show)
            {
                //if (PlayerPrefs.GetInt("Reward", 0) == 1 || PlayerPrefs.GetInt("AvailGift", 0) > 0)
                //{                   
                    GiftCanvas.SetActive(true);
                    GIftManager.giftManagerRef.Start();
                //}                
            }
            else
            {
                GiftCanvas.SetActive(false);
            }
        }

        public void UpdateDiamondKeyText()
        {
            TotalDiamond.text = Util.GetDiamond().ToString(); 
            TotalKey.text = Util.GetKey().ToString();
            //add animation for diamond and key spreding
        }

        public void UnlockLevel(int LevelIndexx)
        {

            PlayerPrefs.SetInt("LevelIndex", LevelIndexx);
            PlayerPrefs.Save();
            LevelIndex = LevelIndexx;
            ShowUnlockLevel(true);
        }

        public void ShowUnlockLevel(bool show)
        {
            LevelUnlockCanvas.SetActive(show);
        }

        private int GetPriceFromLevel(int levelIndex)
        {
            int Price = 0;
            switch (levelIndex)
            {
                case 1:
                    Price = 0;
                    break;
                case 2:
                    Price = 1000;
                    break;
                case 3:
                    Price = 2000;
                    break;
                case 4:
                    Price = 5000;
                    break;
                case 5:
                    Price = 10000;
                    break;
                case 6:
                    Price = 15000;
                    break;
                case 7:
                    Price = 20000;
                    break;
            }
            return Price;
        }

        public void ShowLeaderBoardUi()
        {
            if (GameServices.IsInitialized())
            {
                GameServices.ShowLeaderboardUI();
            }
            else
            {
#if UNITY_ANDROID
                GameServices.Init();    // start a new initialization process
#elif UNITY_IOS
                Debug.Log("Cannot show leaderboard UI: The user is not logged in to Game Center.");
#endif
            }
        }

        public void ShowAchievementUi()
        {
            // Check for initialization before showing achievements UI
            if (GameServices.IsInitialized())
            {
                GameServices.ShowAchievementsUI();
            }
            else
            {
#if UNITY_ANDROID
                GameServices.Init();    // start a new initialization process
#elif UNITY_IOS
    Debug.Log("Cannot show achievements UI: The user is not logged in to Game Center.");
#endif
            }
        }
    }   
}
