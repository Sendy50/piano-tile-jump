﻿

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

//#if AADOTWEEN
using DG.Tweening;
using TMPro;
//#endif

namespace AppAdvisory.Boing
{
    public class Platform : MonoBehaviour
    {
        GameManager gameManager;
        public static Platform Instance;
        public Transform jumpEffect;
        public Transform cube;
        public GameObject diamond;
        public GameObject key;
        public GameObject JumpPower;
        private Vector3 DiamondPos;

        //public TMP_Text BoostText;
        //public ParticleSystem particals;

        void Awake()
        {
            this.gameManager = FindObjectOfType<GameManager>();
            //LocalBoostMode = gameManager.BoostMode;
            //jumpEffect.transform.localScale = Vector3.zero;
            //jumpEffect.gameObject.SetActive(false);
            diamond.SetActive(false);
            key.SetActive(false);
            //BoostText.gameObject.SetActive(false);
            JumpPower.SetActive(false);
            Instance = this;
        }

        void Start()
        {
            StartCoroutine(CoroutDestroyIfNotVisible());
            //particals.Play();
        }

        private void Update()
        {
            
        }


        public void OnPlayerBounce()
        {
            jumpEffect.transform.localScale = Vector3.zero;
            jumpEffect.gameObject.SetActive(true);

            //#if AADOTWEEN
            jumpEffect.DOScale(new Vector3(4f, 4f, 0.3f), 0.2f).SetLoops(2, LoopType.Yoyo);
            //transform.DOMoveY(-0.3f,0.2f).SetLoops(2,LoopType.Yoyo);
            //#endif

            if (JumpPower.activeInHierarchy)
            {
                gameManager.JumpPowered();
                JumpPower.SetActive(false);
            }

            if (diamond.activeInHierarchy )
            {
                gameManager.Add1Diamond();
                diamond.SetActive(false);
            }

            if (key.activeInHierarchy)
            {
                gameManager.Add1Key();
                key.SetActive(false);
            }
        }

        IEnumerator CoroutDestroyIfNotVisible()
        {
            if (gameManager.point > 0)
            {
                //transform.position = new Vector3(transform.position.x, 10f, transform.position.z);
                transform.position = new Vector3(transform.position.x, 92f, transform.position.z);

                //PlateformChange
                //#if AADOTWEEN
                //transform.DOMoveY(0,0.2f); 
                transform.DOMoveY(88.95f, 0.2f);
                //transform.DOMoveY(90.95f, 0.2f);
                //#endif

            }


            //For key
            //if (gameManager.point - gameManager.keyTempPoint >= Util.GetRandomNumber(20f, 30f))
            if (gameManager.spawnPlatformCount > 15 && gameManager.spawnPlatformCount % 42 == 0)
            {
                print("Key Active....>>>");
                key.SetActive(true);
                gameManager.keyTempPoint = gameManager.point;
            }
            else if (gameManager.spawnPlatformCount > 15 && gameManager.spawnPlatformCount % 48 == 0)
            {

                if (!gameManager.JumpPower && (gameManager.spawnPlatformCount - gameManager.TempCount) > 100)
                {
                    gameManager.TempCount = gameManager.spawnPlatformCount;
                    JumpPower.SetActive(true);
                    gameManager.JumppedPower = true;
                }
                else if (!gameManager.JumpPower && gameManager.TempCount == 0)
                {
                    gameManager.TempCount = gameManager.spawnPlatformCount;
                    JumpPower.SetActive(true);
                    gameManager.JumppedPower = true;
                }
            }
            else if (gameManager.spawnPlatformCount > 10 && Util.GetRandomNumber(0f, 100f) < 40f)
            {          
                    diamond.SetActive(!(GameManager.gameManagerRef.JumpPower));             
            }

           // diamond.SetActive(true);
            if (GameManager.gameManagerRef.JumpPower)
            {
                //print("transform.position.z - "+ transform.position.z + "gameManager.camGame.transform.position.z - " + gameManager.camGame.transform.position.z);
                if (transform.position.z - gameManager.playerParent.transform.position.z > 5)
                {
                    if (diamond.activeInHierarchy)
                    {
                        //print("transform.position.z - gameManager.camGame.transform.position.z" + (transform.position.z - gameManager.camGame.transform.position.z));
                        gameManager.Add1Diamond();
                        diamond.SetActive(false);
                    }
                    else if (key.activeInHierarchy)
                    {
                        gameManager.Add1Key();
                        key.SetActive(false);
                    }
                }              
            }




            //diamond.SetActive(true);

            if (GameManager.gameManagerRef.boostLowSpeed <= 0.19f)
                yield return new WaitForSeconds(0.5f);
            else
                yield return new WaitForSeconds(1f);

            //yield return new WaitForSeconds(1f);
            //yield return new WaitForSeconds(.5f);

            while (true)
            {
                //gameManager.TileposZTarget
                if (gameManager.camGame.transform.position.z - transform.position.z > 10)
                {
                    DODestroy();
                    break;
                }
                yield return new WaitForSeconds(0.25f); //.3f
            }
        }

        void DODestroy()
        {
            StopAllCoroutines();
            gameManager.SpawnPlatform(this.transform);
            //diamond.transform.position = new Vector3(0f, 0.32f, this.transform.position.z);
            //key.transform.position = new Vector3(0f, 0.32f, this.transform.position.z);
            //diamond.transform.localScale = new Vector3(20f, 20f, 20f);
            //key.transform.localScale = new Vector3(0.2456f, 0.2456f, 0.2456f);
            StartCoroutine(CoroutDestroyIfNotVisible());
        }
    }
}