﻿using AppAdvisory.Boing;
using EasyMobile;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GIftManagerForLevel : MonoBehaviour
{
    public GameObject GrabedDiamondAndKeyPanel;
    public GameObject AllGiftPanel;
    public GameObject KeyAnimImage;
    public GameObject DiamondAndKeyImage;
    public GameObject DiamondAnimImage;
    public GameObject OpenBoxAnimImage;
    public GameObject OpenBoxImage;
    public TMP_Text DiamondText;
    public TMP_Text KeyText;
    public Text BtnText;
    public Text ExtraBtnText;
    public Button OpenBtn;
    public Button NoThanksBtn;
    public Button ExtraAdBtn;
    private bool openBtn = true;
    private AnimationClip anim;
    private float currCountdownValue;
    private float currCountdownValue1;
    private int GiftDiamond;
    private int GiftKey;
    private int GiftType;
    public static GIftManagerForLevel giftManagerForLevelRef;

    private void Awake()
    {
        giftManagerForLevelRef = this;
    }


    private void Update()
    {
        //if (BtnText.text == "Collect")
        //{
        //    OpenBtn.transform.GetChild(1).gameObject.SetActive(true);
        //    //BtnText.transform.position = new Vector2(35f, -7.625f);
        //    BtnText.GetComponent<RectTransform>().anchoredPosition = new Vector2(60f, -7.625f);
        //}
        //else
        //{
        //    OpenBtn.transform.GetChild(1).gameObject.SetActive(false);
        //    //BtnText.transform.position = new Vector2(0f, -7.625f);
        //    BtnText.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, -7.625f);
        //}

        if (OpenBtn.transform.GetChild(1).gameObject.activeInHierarchy)
            OpenBtn.transform.GetChild(1).gameObject.SetActive(false);

        if (BtnText.GetComponent<RectTransform>().anchoredPosition.x != 0)
            BtnText.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, -7.625f);
    }

    public void ShowGift(int GiftType, int Diamond, int Key = 0)
    {
        print("Gift Typeee --> "+GiftType);
        this.GiftType = GiftType;
        if (GiftType == 1)
        {
            GiftDiamond = Diamond;
            GiftKey = Key;

        }
        else if (GiftType == 2)
        {
            GiftDiamond = Diamond;
            GiftKey = Key;
        }
        else if (GiftType == 3)
        {
            GiftDiamond = Diamond;
            GiftKey = Key;
        }
    }

    

    public void BtnClick()
    {
        StartCoroutine(OpenBtnClick());

//        print("Open Btn Click "+openBtn);
//        if (openBtn)
//        {
//            if (Advertising.IsRewardedAdReady())
//            {
//                AdsManagerForLevel.Instance.ShowRewardAds(77);
//            }
//            else
//            {
//#if UNITY_EDITOR
//                print("Reward isn't Available In Editor");
//#elif UNITY_ANDROID
//            NativeUI.ShowToast("Reward isn't Available");
//#elif UNITY_IPHONE || UNITY_IOS
//            NativeUI.Alert("Failed","Reward isn't Available","ok");
//#endif
//            }
//        }
//        else
//        {
//            StartCoroutine(OpenBtnClick());
//        }
        
        //StartCoroutine(OpenBtnClick());
    }


    public void NoThanksBtnClick()
    {
        CloseButton();
    }

    public void CloseButton()
    {
        print("close Button");
        DiamondText.text = "0";
        KeyText.text = "0";
        GrabedDiamondAndKeyPanel.SetActive(false);
        OpenBoxAnimImage.SetActive(false);
        DiamondAndKeyImage.SetActive(false);
        DiamondAnimImage.SetActive(false);
        KeyAnimImage.SetActive(false);
        OpenBoxImage.SetActive(true);
        OpenBtn.gameObject.SetActive(true);
        ExtraAdBtn.gameObject.SetActive(false);
        NoThanksBtn.gameObject.SetActive(true);
        BtnText.text = "Open";
        openBtn = true;

        GameManager.gameManagerRef.SetOutScreenData();
    }


    public IEnumerator OpenBtnClick()
    {
        if (openBtn)
        {
            print("Open Btn Click2 " + openBtn);
            GameManager.gameManagerRef.PlaySoundJump();
            GrabedDiamondAndKeyPanel.SetActive(true);
            NoThanksBtn.gameObject.SetActive(false);
            BtnText.text = "Collect";
            ExtraBtnText.text = GiftType == 3 ? "Extra x2" : "Extra x4";
            OpenBoxImage.SetActive(false);
            openBtn = false;
            OpenBoxAnimImage.SetActive(true);
            DiamondAndKeyImage.SetActive(true);
            DiamondAndKeyImage.GetComponent<Image>().sprite = Resources.Load<Sprite>(GiftType == 1 ? "Button/diamond 1" : GiftType == 2 ? "Button/Mix" : "Button/chavi 1");
            OpenBtn.gameObject.SetActive(false);


            yield return new WaitForSeconds(1f);
            DiamondAnimImage.SetActive(GiftType == 1 || GiftType == 2);
            KeyAnimImage.SetActive(GiftType == 3 || GiftType == 2);
            StartCoroutine(AddDiamond(GiftDiamond,true));
            StartCoroutine(AddKey(GiftKey,true));
        }
        else
        {
            GameManager.gameManagerRef.CoinDropSound();
            Util.SetDiamond(Util.GetDiamond() + GiftDiamond);
            Util.SetKey(Util.GetKey() + GiftKey);
            OpenBtn.gameObject.SetActive(false);


            AdsManagerForLevel.Instance.ShowInterstialAds(1);

            yield return new WaitForSeconds(0.2f);
           
            //MainScreenManager.mainScreenManagerRef.UpdateDiamondKeyText();

            DiamondText.text = "0";
            KeyText.text = "0";
            GrabedDiamondAndKeyPanel.SetActive(false);
            OpenBoxAnimImage.SetActive(false);
            DiamondAndKeyImage.SetActive(false);
            DiamondAnimImage.SetActive(false);
            KeyAnimImage.SetActive(false);
            OpenBoxImage.SetActive(true);
            OpenBtn.gameObject.SetActive(true);
            ExtraAdBtn.gameObject.SetActive(false);
            BtnText.text = "Open";
            NoThanksBtn.gameObject.SetActive(true);
            openBtn = true;

            GameManager.gameManagerRef.SetOutScreenData();           
        }
    }


    public IEnumerator ReturnToExtraRewardAdd()
    {
        OpenBoxAnimImage.SetActive(false);
        DiamondAndKeyImage.SetActive(false);
        ExtraAdBtn.gameObject.SetActive(false);
        print("call Extra x4");
        GrabedDiamondAndKeyPanel.SetActive(true);
        NoThanksBtn.gameObject.SetActive(false);
        BtnText.text = "Collect";
        OpenBoxImage.SetActive(false);
        openBtn = false;
        OpenBoxAnimImage.SetActive(true);
        DiamondAndKeyImage.SetActive(true);
        DiamondAndKeyImage.GetComponent<Image>().sprite = Resources.Load<Sprite>(GiftType == 1 ? "Button/diamond 1" : GiftType == 2 ? "Button/Mix" : "Button/chavi 1");
        OpenBtn.gameObject.SetActive(false);

        yield return new WaitForSeconds(1f);
        DiamondAnimImage.SetActive(GiftType == 1 || GiftType == 2);
        KeyAnimImage.SetActive(GiftType == 3 || GiftType == 2);
        GiftDiamond *= 4;
        GiftKey *= 2;
        StartCoroutine(AddDiamond(GiftDiamond, false));
        StartCoroutine(AddKey(GiftKey, false));
    }



    public IEnumerator AddDiamond(float countdownValue, bool extra)
    {
        currCountdownValue = 0;
        while (currCountdownValue < countdownValue)
        {           
            //Debug.Log("Countdown: " + currCountdownValue);
            yield return new WaitForSeconds(0.5f);
            currCountdownValue += countdownValue / 5;
            DiamondText.text = currCountdownValue.ToString("0");
            GameManager.gameManagerRef.CoinDropSound();
        }

        if (currCountdownValue >= countdownValue )
        {
            print("Open Btn visible true ");
            DiamondAnimImage.SetActive(false);
            KeyAnimImage.SetActive(false);           
            OpenBtn.gameObject.SetActive(true);
            ExtraAdBtn.gameObject.SetActive(extra);
            StopAllCoroutines();
        }
    }

    public IEnumerator AddKey(float countdownValue , bool extra)
    {
        currCountdownValue1 = 0;
        while (currCountdownValue1 < countdownValue)
        {

            //Debug.Log("Countdown: " + currCountdownValue1);
            yield return new WaitForSeconds(extra == true ? 0.01f : 0.01f);
            currCountdownValue1 += countdownValue/5;
            KeyText.text = currCountdownValue1.ToString("0");
            GameManager.gameManagerRef.CoinDropSound();
        }

        if (currCountdownValue1 >= countdownValue && GiftType == 3)
        {
            print("Open Btn visible true ");
            DiamondAnimImage.SetActive(false);
            KeyAnimImage.SetActive(false);          
            OpenBtn.gameObject.SetActive(true);
            ExtraAdBtn.gameObject.SetActive(extra);
            StopAllCoroutines();
        }
    }
}
