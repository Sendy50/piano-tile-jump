﻿using AppAdvisory.Boing;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DailyRewardManger : MonoBehaviour
{
    public Image Day1Panel;
    public Image Day2Panel;
    public Image Day3Panel;
    public Image Day4Panel;
    public Image Day5Panel;
    public Image Day6Panel;
    public Image Day7Panel;
    public TMP_Text NextRewardTimeText;
    private static System.DateTime startDate;
    private static System.DateTime today;
    private int TotalDays;
    private DateTime LastRewardedTime;
    private TimeSpan DurationUnit;
    private DateTime _24HourRewardTime;
    private bool RewardAvailable;
    private String Hour;
    private String Minute;
    private String Second;


    // Start is called before the first frame update
    void Start()
    {

        //PlayerPrefs.DeleteAll();


        RewardAvailable = PlayerPrefs.GetInt("RewardAvailable", 1) == 1;
        //startDate = System.Convert.ToDateTime(PlayerPrefs.GetString("DateInitialized"));

       // TotalDays = MainScreenManager.mainScreenManagerRef.TodaysDay;




        //if (TotalDays != PlayerPrefs.GetInt("LastRewardDay",0))
        //{
        //    EnabledSelectedReward(TotalDays);
        //}

        //Temp Code For Time Different

        //end
    }

    private void Update()
    {
        if (RewardAvailable)
        {
            EnabledSelectedReward(PlayerPrefs.GetInt("NextRewardDay", 1));
            NextRewardTimeText.text = "Your Reward is Ready..";
        }
        else if(PlayerPrefs.HasKey("LastRewardTime"))
        {
            EnabledSelectedReward(0);
           // print(string.Format("LastRewardTime :  {0} :, {1} :, {2} :", DateTime.Parse(PlayerPrefs.GetString("LastRewardTime")).Hour, DateTime.Parse(PlayerPrefs.GetString("LastRewardTime")).Minute, DateTime.Parse(PlayerPrefs.GetString("LastRewardTime")).Second));
            LastRewardedTime = DateTime.Parse(PlayerPrefs.GetString("LastRewardTime"));
            DurationUnit = DateTime.Now.Subtract(LastRewardedTime);

            DateTime Temp = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            //DateTime Temp = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 1, 00);
            _24HourRewardTime = Temp.Subtract(DurationUnit);
            NextRewardTimeText.text = string.Format("Next Reward in :  {0} : {1} : {2}", (_24HourRewardTime.Hour < 10) ? "0" + _24HourRewardTime.Hour.ToString("f0") : _24HourRewardTime.Hour.ToString("f0"), (_24HourRewardTime.Minute < 10) ? "0" + _24HourRewardTime.Minute.ToString("f0") : _24HourRewardTime.Minute.ToString("f0"), (_24HourRewardTime.Second < 10) ? "0" + _24HourRewardTime.Second.ToString("f0") : _24HourRewardTime.Second.ToString("f0"));

            if (_24HourRewardTime < new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0))
            {
                RewardAvailable = true;
                PlayerPrefs.SetInt("RewardAvailable", 1); // true == 1, false == 0
                PlayerPrefs.Save();
            }
        }
    }




    private void EnabledSelectedReward(int totalDays)
    {
        //print("CountTodayDay :" + totalDays);
        Day1Panel.gameObject.SetActive(!(totalDays == 1));
        Day2Panel.gameObject.SetActive(!(totalDays == 2));
        Day3Panel.gameObject.SetActive(!(totalDays == 3));
        Day4Panel.gameObject.SetActive(!(totalDays == 4));
        Day5Panel.gameObject.SetActive(!(totalDays == 5));
        Day6Panel.gameObject.SetActive(!(totalDays == 6));
        Day7Panel.gameObject.SetActive(!(totalDays == 7));
    }


    
   
    public void ClaimBtnClick(int Day)
    {
        PlayerPrefs.SetInt("Reward", 1);
        PlayerPrefs.SetInt("LastRewardDay", Day);
        PlayerPrefs.SetString("LastRewardTime", System.DateTime.Now.ToString());
        PlayerPrefs.Save();
        RewardAvailable = false;
        PlayerPrefs.SetInt("RewardAvailable", 0); // true == 1, false == 0
        PlayerPrefs.Save();

        MainScreenManager.mainScreenManagerRef.OpenGift(true);

        Day++;

        if (Day == 8)
        {
            PlayerPrefs.SetInt("NextRewardDay", 1);      
        }
        else
        {       
            PlayerPrefs.SetInt("NextRewardDay", Day);         
        }

        PlayerPrefs.Save();
        StartCoroutine(MainScreenManager.mainScreenManagerRef.OpenDailyReward(false));
    }
}
