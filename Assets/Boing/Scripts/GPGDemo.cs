﻿using EasyMobile;
using GooglePlayGames;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GPGDemo : MonoBehaviour
{
    //#region PUBLIC_VAR
    //public string leaderboard;
    //public string achievement;
    //public static GPGDemo Instance;
    //public void Awake()
    //{
    //    Instance = this;
    //}

    //void Start()
    //{
    //    // recommended for debugging:
    //    PlayGamesPlatform.DebugLogEnabled = true;

    //    // Activate the Google Play Games platform
    //    PlayGamesPlatform.Activate();

    //    LogIn();
    //}
    //#endregion
    //#region BUTTON_CALLBACKS
    ///// <summary>
    ///// Login In Into Your Google+ Account
    ///// </summary>
    ///// 

    //public void LogIn()
    //{
    //    Social.localUser.Authenticate((bool success) =>
    //    {
    //        if (success)
    //        {
    //            Debug.Log("Login Sucess");
    //        }
    //        else
    //        {
    //            Debug.Log("Login failed");
    //        }
    //    });
    //}
    ///// <summary>
    ///// Shows All Available Leaderborad
    ///// </summary>
    //public void OnShowLeaderBoard()
    //{
    //    //Social.ShowLeaderboardUI (); // Show all leaderboard
    //    ((PlayGamesPlatform)Social.Active).ShowLeaderboardUI(); // Show current (Active) leaderboard
    //}
    ///// <summary>
    ///// Adds Score To leader board
    ///// </summary>
    //public void OnAddScoreToLeaderBorad(int Score, string LeaderBoardId)
    //{
    //    if (Social.localUser.authenticated)
    //    {
    //        Social.ReportScore(Score, LeaderBoardId, (bool success) =>
    //        {
    //            if (success)
    //            {
    //                Debug.Log("Update Score Success");
    //                NativeUI.ShowToast("Update Score Success");
    //            }
    //            else
    //            {
    //                Debug.Log("Update Score Fail");
    //                NativeUI.ShowToast("Update Score Fail");
    //            }
    //        });
    //    }
    //}
    ///// <summary>
    ///// On Logout of your Google+ Account
    ///// </summary>
    ///// 

    //public void OnShowAchievement()
    //{
    //    //Social.ShowLeaderboardUI (); // Show all leaderboard
    //    ((PlayGamesPlatform)Social.Active).ShowAchievementsUI(); // Show current (Active) leaderboard
    //}

    //public void OnLogOut()
    //{
    //    ((PlayGamesPlatform)Social.Active).SignOut();
    //}
    //#endregion
}
