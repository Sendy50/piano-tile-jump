﻿

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
//#if AADOTWEEN
using DG.Tweening;
using EasyMobile;
//#endif
#if UNITY_5_3_OR_NEWER
using UnityEngine.SceneManagement;
#endif


namespace AppAdvisory.Boing
{
	/// <summary>
	/// Utility class.
	/// </summary>
	public static class Util
	{
		static System.Random random = new System.Random();
        private static string[] CurrectLevelName = { "", "The Ice", "City Light" , "Helloween", "Down To Hell","Travel To Brazil", "Travel To London", "Travel To NewYork", "Travel To Sydney", "Travel To Beijing", "Travel To Paris", "Travel To Moscow", "Travel To Berlin", "Travel To Roma" };
        private static DateTime LastRewardedTime;
        private static TimeSpan DurationUnit;
        private static DateTime _24HourRewardTime;
        private static TimeSpan UnlockTimeTemp;
        internal static int Diamond4GIft;
        internal static int Key4GIft;

        public static double GetRandomNumber(double minimum, double maximum)
		{ 
			return random.NextDouble() * (maximum - minimum) + minimum;
		}

		public static float GetRandomNumber(float minimum, float maximum)
		{ 
			return (float)random.NextDouble() * (maximum - minimum) + minimum;
		}

		/// <summary>
		/// Real shuffle of List
		/// </summary>
		public static void Shuffle<T>(this IList<T> list)  
		{  
			int n = list.Count;  
			while (n > 1) {  
				n--;  
				int k = random.Next(n + 1);  
				T value = list[k];  
				list[k] = list[n];  
				list[n] = value;  
			}  
		}

		public static void SetLastScore(int score)
		{
			PlayerPrefs.SetInt("_LASTSCORE",score);
			PlayerPrefs.Save();
			SetBestScore(score);

            UnlockScoreAchievement(score);
        }

        private static void UnlockScoreAchievement(int score)
        {   
            if (score >= 1000 && PlayerPrefs.GetInt("AchiScore1000", 0) == 0)
            {
                GameServices.UnlockAchievement(EM_GameServicesConstants.Achievement_AchivementScore1000);
                PlayerPrefs.SetInt("AchiScore1000", 1);
                PlayerPrefs.Save();
                AchievementPlusGift(500,10);
            }
            else if (score >= 500 && PlayerPrefs.GetInt("AchiScore500", 0) == 0)
            {
                GameServices.UnlockAchievement(EM_GameServicesConstants.Achievement_AchivementScore500);
                PlayerPrefs.SetInt("AchiScore500", 1);
                PlayerPrefs.Save();
                AchievementPlusGift(250, 5);
            }
            else if (score >= 250 && PlayerPrefs.GetInt("AchiScore250", 0) == 0)
            {
                GameServices.UnlockAchievement(EM_GameServicesConstants.Achievement_AchivementScore250);
                PlayerPrefs.SetInt("AchiScore250", 1);
                PlayerPrefs.Save();
                AchievementPlusGift(100, 2);
            }
            else if (score >= 100 && PlayerPrefs.GetInt("AchiScore100", 0) == 0)
            {
                GameServices.UnlockAchievement(EM_GameServicesConstants.Achievement_AchivementScore100);
                PlayerPrefs.SetInt("AchiScore100", 1);
                PlayerPrefs.Save();
                AchievementPlusGift(50,1);
            }    
        }

        public static void SetDiamond(int diamond)
		{
			PlayerPrefs.SetInt("_DIAMOND",diamond);
			PlayerPrefs.Save();
		}

		public static int GetDiamond()
		{
			return PlayerPrefs.GetInt("_DIAMOND", 0);
		}


        public static void SetKey(int key)
        {
            PlayerPrefs.SetInt("_KEY", key);
            PlayerPrefs.Save();
        }

        public static int GetKey()
        {
            return PlayerPrefs.GetInt("_KEY", 0);
        }

        static void SetBestScore(int score)
		{
			int b = GetBestScore();

            if (score > b)
            {
                PlayerPrefs.SetInt("_BESTSCORE" + GetLevelIndex(), score);
                PlayerPrefs.Save();
                if (score > 150 && PlayerPrefs.GetInt("RatingOne", 1) == 1)
                {
                    StoreReview.RequestRating();
                    PlayerPrefs.SetInt("RatingOne", 0);
                    PlayerPrefs.Save();
                }
                else if (score > 550 && PlayerPrefs.GetInt("RatingOne", 1) == 0)
                {
                    StoreReview.RequestRating();
                    PlayerPrefs.SetInt("RatingOne", 1);
                    PlayerPrefs.Save();
                }
            }           
		}

		public static int GetBestScore()
		{
			return PlayerPrefs.GetInt("_BESTSCORE" + GetLevelIndex(), 0);
		}

		public static int GetLastScore()
		{
			return PlayerPrefs.GetInt("_LASTSCORE",0);
		}

		/// <summary>
		/// Clean the memory and reload the scene
		/// </summary>
		public static void ReloadLevel()
		{
			CleanMemory();

			#if UNITY_5_3_OR_NEWER
			SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex,LoadSceneMode.Single);
			#else
			Application.LoadLevel(Application.loadedLevel);
			#endif

			CleanMemory();
		}
		/// <summary>
		/// Clean the memory
		/// </summary>
		public static void CleanMemory()
		{
			//#if AADOTWEEN
			DOTween.KillAll();
			//#endif
			GC.Collect();
			Application.targetFrameRate = 60;
		}

		public static void SetAlpha(this Text text, float alpha)
		{
			Color c = text.color;
			c.a = alpha;
			text.color = c;
		}

		public static void SetScaleX(this RectTransform rect, float scale)
		{
			var s = rect.localScale;
			s.x = scale;
			rect.localScale = s;
		}

        public static string GetLevelName()
        {
            return CurrectLevelName[GetLevelIndex()];
        }


        public static int GetLevelIndex()
        {
            return PlayerPrefs.GetInt("LevelIndex",1); 
        }

        internal static int GetBaseImage(int point)
        {
            if(point > GetRandomNumber(400, 550))
            {
                //3 star
                setMaxBaseImage(3);
                return 3;
            }
            else if (point > GetRandomNumber(201, 300))
            {
                //2 star
                setMaxBaseImage(2);
                return 2;
            }
            else if (point > GetRandomNumber(50, 51))
            {
                //1 star
                setMaxBaseImage(1);
                return 1;
            }
            else
            {
                //0 star
                setMaxBaseImage(0);
                return 0;
            }
        }

        public static void setMaxBaseImage(int index)
        {
            if (index > PlayerPrefs.GetInt("MaxBase" + GetLevelIndex(), 0))
            {
                PlayerPrefs.SetInt("MaxBase" + GetLevelIndex(), index);
                PlayerPrefs.Save();
            }       
        }

        public static int PlusGift(int Type)
        {
            int temp = PlayerPrefs.GetInt("AvailGift", 0);
            temp++;
            PlayerPrefs.SetInt("AvailGift",temp);
            PlayerPrefs.SetInt("GiftType" + temp,Type);
            PlayerPrefs.Save();
            return Type;
        }

        public static void AchievementPlusGift(int diamond, int key)
        {
            int temp = PlayerPrefs.GetInt("AvailGift", 0);
            temp++;
            Diamond4GIft = diamond;
            Key4GIft = key;
            PlayerPrefs.SetInt("AvailGift", temp);
            PlayerPrefs.SetInt("GiftType" + temp, 4);
            PlayerPrefs.SetInt("Diamond4Gift",diamond);
            PlayerPrefs.SetInt("Key4Gift", key);
            PlayerPrefs.Save();
           
        }

        public static int GetGiftType()
        {
            int temp = PlayerPrefs.GetInt("AvailGift", 0);
            return PlayerPrefs.GetInt("GiftType" + temp, 0);
        }

        public static void MinusGift()
        {            
            int temp = PlayerPrefs.GetInt("AvailGift", 0);
            PlayerPrefs.SetInt("GiftType" + temp, 0);
            PlayerPrefs.Save();
            temp--;
            PlayerPrefs.SetInt("AvailGift", temp);
            PlayerPrefs.Save();
        }

        public static bool RewardIsAvailable()
        {
            bool IsAvailable = false;

            //if (!(PlayerPrefs.HasKey("LastRewardTime")))
            //{
            //    return true;
            //}           

            LastRewardedTime = DateTime.Parse(PlayerPrefs.GetString("LastRewardTime", DateTime.Now.AddDays(-1).ToString()));
            DurationUnit = DateTime.Now.Subtract(LastRewardedTime);

            DateTime Temp = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            //DateTime Temp = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 1, 00);
            _24HourRewardTime = Temp.Subtract(DurationUnit);

            if (_24HourRewardTime < new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0))
            {
                IsAvailable = true;
            }
            
            return IsAvailable;
            //return true;
        }

        public static bool CheckDiamondAvailable(int diamondd)
        {
            return GetDiamond() > diamondd;
        }



        //public static Sprite GetTotalHeart(int LevelIndex)
        //{
        //    if(PlayerPrefs.GetInt(LevelIndex + "_LevelPlayCounter", 0) == 1){
        //        return Resources.Load<Sprite>("Button/heart1");
        //    }
        //    else if (PlayerPrefs.GetInt(LevelIndex + "_LevelPlayCounter", 0) == 2)
        //    {
        //        return Resources.Load<Sprite>("Button/heart2");
        //    }
        //    else if (PlayerPrefs.GetInt(LevelIndex + "_LevelPlayCounter", 0) == 3)
        //    {
        //        return Resources.Load<Sprite>("Button/heart3");
        //    }
        //    else
        //    {
        //        return Resources.Load<Sprite>("Button/heart0");
        //    }
        //}




        public static string GetLevelUnlockTime(int LevelIndex)
        {
            
            //PlayerPrefs.SetString(LevelIndex + "_LevelLockTime", DateTime.Now.AddHours(3).ToString());
            // PlayerPrefs.Save();
            //DateTime Tempp = DateTime.Parse(PlayerPrefs.GetString(LevelIndex + "_LevelLockTime",DateTime.Now.AddHours(3).ToString()));
            DateTime Tempp = DateTime.Parse(PlayerPrefs.GetString(LevelIndex + "_LevelLockTime"));
            //if (PlayerPrefs.GetInt(LevelIndex + "_LevelBuy", 0) == 1 || PlayerPrefs.GetInt(LevelIndex + "_LevelPlayCounter", 0) == 3)
            //{
            //    MainScreenManager.mainScreenManagerRef.UnlockLevelChecker(LevelIndex);
            //}
           // Debug.Log(LevelIndex + " $$$$$$$$$   Temp time : "+ Tempp);
            //if (DateTime.Now < Tempp) {
            UnlockTimeTemp = Tempp.Subtract(DateTime.Now);

           // Debug.Log(LevelIndex + "$$$$$$$$$   UnlockTimeTemp : " + UnlockTimeTemp);
            if (UnlockTimeTemp < TimeSpan.Zero)
            {
                PlayerPrefs.SetInt(LevelIndex + "_Unlock", 1);
                PlayerPrefs.SetString(LevelIndex + "_LevelLockTime", DateTime.Now.AddHours(3).ToString());
                PlayerPrefs.Save();
                //NativeUI.ShowToast("Level Unlock");
                return "00:00:00";
            }
            else
            {
                return string.Format("{0} : {1} : {2}", (UnlockTimeTemp.Hours < 10) ? "0" + UnlockTimeTemp.Hours.ToString("f0") : UnlockTimeTemp.Hours.ToString("f0"), (UnlockTimeTemp.Minutes < 10) ? "0" + UnlockTimeTemp.Minutes.ToString("f0") : UnlockTimeTemp.Minutes.ToString("f0"), (UnlockTimeTemp.Seconds < 10) ? "0" + UnlockTimeTemp.Seconds.ToString("f0") : UnlockTimeTemp.Seconds.ToString("f0"));
            }         
        }     
    }


    
}