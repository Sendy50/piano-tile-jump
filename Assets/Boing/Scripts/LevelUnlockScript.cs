﻿using AppAdvisory.Boing;
using EasyMobile;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelUnlockScript : MonoBehaviour
{
    public int[] Diamonds;
    public int[] Days;

 
   

    public void UnlockLevelByDIamond(int Index)
    {
        int Level = Util.GetLevelIndex();

        print("Level Unlock : "+ Level);

        if (Index == 6 && Util.GetDiamond() > Diamonds[Index-1] && Util.GetKey() > 300)
        {
            PlayerPrefs.SetInt("_DIAMOND", Util.GetDiamond() - Diamonds[Index-1]);
            PlayerPrefs.SetInt("_KEY", Util.GetKey() - 300);
            PlayerPrefs.SetInt(Level + "_Unlock", 1);
            PlayerPrefs.SetInt(Level + "_LevelBuy", 1);
            PlayerPrefs.SetString(Level + "_LevelLockTime", DateTime.Now.AddDays(+Days[Index-1]).ToString());
            PlayerPrefs.Save();

#if UNITY_EDITOR
            print("Level Unlock Success For "+ Days[Index - 1]+" Days");
#elif UNITY_ANDROID
            NativeUI.ShowToast("Level Unlock Success For "+ Days[Index - 1]+" Days",true);
#elif UNITY_IPHONE || UNITY_IOS
            NativeUI.Alert("Level Unlock","Level Unlock Success For "+ Days[Index - 1]+" Days","ok");
#endif


            //NativeUI.ShowToast("Level Unlock Success For "+ Days[Index - 1]+" Days",true);
            MainScreenManager.mainScreenManagerRef.ShowUnlockLevel(false);
            UnlockAchievement();
        }
        else if (Index != 6 && Util.GetDiamond() > Diamonds[Index-1])
        {
          
            PlayerPrefs.SetInt("_DIAMOND", Util.GetDiamond() - Diamonds[Index-1]);
            PlayerPrefs.SetInt(Level + "_Unlock", 1);
            PlayerPrefs.SetInt(Level + "_LevelBuy", 1);
            PlayerPrefs.SetString(Level + "_LevelLockTime", DateTime.Now.AddDays(+Days[Index-1]).ToString());
            print(Level +  "Level Buy : " + PlayerPrefs.GetString(Level + "_LevelLockTime","00:00:01"));
            PlayerPrefs.Save();

#if UNITY_EDITOR
            print("Level Unlock Success For " + Days[Index - 1] + " Days");
#elif UNITY_ANDROID
            NativeUI.ShowToast("Level Unlock Success For "+ Days[Index - 1]+" Days",true);
#elif UNITY_IPHONE || UNITY_IOS
            NativeUI.Alert("Level Unlock","Level Unlock Success For "+ Days[Index - 1]+" Days","ok");
#endif


            //NativeUI.ShowToast("Level Unlock Success For " + Days[Index - 1] + " Days", true);
            MainScreenManager.mainScreenManagerRef.ShowUnlockLevel(false);

            UnlockAchievement();
        }
        else
        {

#if UNITY_EDITOR
            print("insufficient Diamond");
#elif UNITY_ANDROID
            NativeUI.ShowToast("insufficient Diamond");
#elif UNITY_IPHONE || UNITY_IOS
            NativeUI.Alert("Level Unlock Failed","insufficient Diamond","ok");
#endif
           
            //NativeUI.ShowToast("insufficient Diamond", true);
            print("insufficient Diamond");
            MainScreenManager.mainScreenManagerRef.OpenShop(true);
            MainScreenManager.mainScreenManagerRef.ShowUnlockLevel(false);
        }

        MainScreenManager.mainScreenManagerRef.RefressLevelUi();



        //MainScreenManager.mainScreenManagerRef.UpdateDiamondKeyText();
        //MainScreenManager.mainScreenManagerRef.RefressLevelUi();

    }


    private void UnlockAchievement()
    {
        PlayerPrefs.SetInt("UnlockCounter", (PlayerPrefs.GetInt("UnlockCounter",0) + 1));
        PlayerPrefs.Save();

        if (PlayerPrefs.GetInt("UnlockCounter", 0) >= 100)
        {
            GameServices.UnlockAchievement(EM_GameServicesConstants.Achievement_AchivementLevelUnlock100);
        }
        else if (PlayerPrefs.GetInt("UnlockCounter", 0) >= 50)
        {
            GameServices.UnlockAchievement(EM_GameServicesConstants.Achievement_AchivementLevelUnlock50);
        }
        else if (PlayerPrefs.GetInt("UnlockCounter", 0) >= 10)
        {
            GameServices.UnlockAchievement(EM_GameServicesConstants.Achievement_AchivementLevelUnlock50);
        }
        else if (PlayerPrefs.GetInt("UnlockCounter", 0) >= 5)
        {
            GameServices.UnlockAchievement(EM_GameServicesConstants.Achievement_AchivementLevelUnlock5);
        }
        else if (PlayerPrefs.GetInt("UnlockCounter", 0) >= 1)
        {
            //NativeUI.ShowToast("Unlock One Achievement");
            GameServices.UnlockAchievement(EM_GameServicesConstants.Achievement_AchivementLevelUnlock1);
        }
    }
}
