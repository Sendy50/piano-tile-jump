﻿using AppAdvisory.Boing;
using EasyMobile;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public  class IAPManager : MonoBehaviour
{
    public int[] DiamondsPacksValue;
    public int[] KeyPacksValue;
    public static IAPManager IAPManagerRef;

    private void Awake()
    {

        if (!RuntimeManager.IsInitialized())
        {
            RuntimeManager.Init();
        }

        IAPManagerRef = this;

    }


    private void Start()
    {
        // Initialize the IAP module
        bool isInitialized = InAppPurchasing.IsInitialized();
        Debug.Log("$$$$$$$$$$$$$ isInitialized : $$$$$$$$$$$$$$$$$$$$$ " + isInitialized);

        if (!isInitialized)
        {
            InAppPurchasing.InitializePurchasing();
        }

       

        IAPProduct[] products = InAppPurchasing.GetAllIAPProducts();

        Debug.Log("$$$$$$$$$$$$$ Size Of All IAPProduct : $$$$$$$$$$$$$$$$$$$$$ " + products.Length);

        // Print all product names
        foreach (IAPProduct prod in products)
        {
            Debug.Log("Product name: " + prod.Name +" Product Price "+ prod.Price);
        }
    }

    public void PurchasePack(int Index)
    {
        //NativeUI.ShowToast("Purchase : index click "+ Index);
        switch (Index)
        {
            //for diamond
            case 0:
                print("Purchage for : " + EM_IAPConstants.Product_Small_Pack_Diamond);
                InAppPurchasing.Purchase(EM_IAPConstants.Product_Small_Pack_Diamond);
                break;
            case 1:
                print("Purchage for : " + EM_IAPConstants.Product_Medium_Pack_Diamond);
                InAppPurchasing.Purchase(EM_IAPConstants.Product_Medium_Pack_Diamond);
                break;
            case 2:
                print("Purchage for : " + EM_IAPConstants.Product_Large_Pack_Diamond);
                InAppPurchasing.Purchase(EM_IAPConstants.Product_Large_Pack_Diamond);
                break;
            case 3:
                print("Purchage for : " + EM_IAPConstants.Product_Huge_Pack_Diamond);
                InAppPurchasing.Purchase(EM_IAPConstants.Product_Huge_Pack_Diamond);
                break;


            //for key
            case 4:
                print("Purchage for : " + EM_IAPConstants.Product_Small_Pack_Key);
                InAppPurchasing.Purchase(EM_IAPConstants.Product_Small_Pack_Key);
                break;
            case 5:
                print("Purchage for : " + EM_IAPConstants.Product_Medium_Pack_key);
                InAppPurchasing.Purchase(EM_IAPConstants.Product_Medium_Pack_key);
                break;
            case 6:
                print("Purchage for : " + EM_IAPConstants.Product_Large_Pack_Key);
                InAppPurchasing.Purchase(EM_IAPConstants.Product_Large_Pack_Key);
                break;
            case 7:
                print("Purchage for : " + EM_IAPConstants.Product_Huge_Pack_Key);
                InAppPurchasing.Purchase(EM_IAPConstants.Product_Huge_Pack_Key);
                break;
            case 8:
                print("Purchage for : " + EM_IAPConstants.Product_RemoveAds);


                if (PlayerPrefs.GetInt("RemoveAds", 0) == 0)
                {
                    InAppPurchasing.Purchase(EM_IAPConstants.Product_RemoveAds);
                }
                else
                {
#if UNITY_EDITOR
                    print("Remove Ads Already Purchased");
#elif UNITY_ANDROID
            NativeUI.ShowToast("Remove Ads Already Purchased");
#elif UNITY_IPHONE || UNITY_IOS
            NativeUI.Alert("Process Success","Remove Ads Already Purchased","ok");
#endif
                }

                break;

        }  
    }



    // Subscribe to IAP purchase events
    void OnEnable()
    {
        InAppPurchasing.PurchaseCompleted += PurchaseCompletedHandler;
        InAppPurchasing.PurchaseFailed += PurchaseFailedHandler;

        GameServices.UserLoginSucceeded += OnUserLoginSucceeded;
        GameServices.UserLoginFailed += OnUserLoginFailed;
    }

    // Unsubscribe when the game object is disabled
    void OnDisable()
    {
        InAppPurchasing.PurchaseCompleted -= PurchaseCompletedHandler;
        InAppPurchasing.PurchaseFailed -= PurchaseFailedHandler;

        GameServices.UserLoginSucceeded -= OnUserLoginSucceeded;
        GameServices.UserLoginFailed -= OnUserLoginFailed;
    }


    // Successful purchase handler
    void PurchaseCompletedHandler(IAPProduct product)
    {
        // Compare product name to the generated name constants to determine which product was bought
        switch (product.Name)
        {
            //for DiamondPack
            case EM_IAPConstants.Product_Small_Pack_Diamond:
                Util.SetDiamond(Util.GetDiamond() + DiamondsPacksValue[0]);


#if UNITY_EDITOR
                print(" + " + DiamondsPacksValue[0] + " Diamonds Added Successfull");
#elif UNITY_ANDROID
            NativeUI.ShowToast(" + " + DiamondsPacksValue[0] + " Diamonds Added Successfull");
#elif UNITY_IPHONE || UNITY_IOS
            NativeUI.Alert("Process Success"," + " + DiamondsPacksValue[0] + " Diamonds Added Successfull","ok");
#endif

                //NativeUI.ShowToast(" + " + DiamondsPacksValue[0] + " Diamonds Added Successfull");
                break;
            case EM_IAPConstants.Product_Medium_Pack_Diamond:
                Util.SetDiamond(Util.GetDiamond() + DiamondsPacksValue[1]);


#if UNITY_EDITOR
                print(" + " + DiamondsPacksValue[1] + " Diamonds Added Successfull");
#elif UNITY_ANDROID
            NativeUI.ShowToast(" + " + DiamondsPacksValue[1] + " Diamonds Added Successfull");
#elif UNITY_IPHONE || UNITY_IOS
            NativeUI.Alert("Process Success"," + " + DiamondsPacksValue[1] + " Diamonds Added Successfull","ok");
#endif

                //NativeUI.ShowToast(" + " + DiamondsPacksValue[1] + " Diamonds Added Successfull");
                break;
            case EM_IAPConstants.Product_Large_Pack_Diamond:
                Util.SetDiamond(Util.GetDiamond() + DiamondsPacksValue[2]);

#if UNITY_EDITOR
                print(" + " + DiamondsPacksValue[1] + " Diamonds Added Successfull");
#elif UNITY_ANDROID
            NativeUI.ShowToast(" + " + DiamondsPacksValue[2] + " Diamonds Added Successfull");
#elif UNITY_IPHONE || UNITY_IOS
            NativeUI.Alert("Process Success"," + " + DiamondsPacksValue[2] + " Diamonds Added Successfull","ok");
#endif

                //NativeUI.ShowToast(" + " + DiamondsPacksValue[2] + " Diamonds Added Successfull");
                break;
            case EM_IAPConstants.Product_Huge_Pack_Diamond:
                Util.SetDiamond(Util.GetDiamond() + DiamondsPacksValue[3]);

#if UNITY_EDITOR
                print(" + " + DiamondsPacksValue[3] + " Diamonds Added Successfull");
#elif UNITY_ANDROID
            NativeUI.ShowToast(" + " + DiamondsPacksValue[3] + " Diamonds Added Successfull");
#elif UNITY_IPHONE || UNITY_IOS
            NativeUI.Alert("Process Success"," + " + DiamondsPacksValue[3] + " Diamonds Added Successfull","ok");
#endif

                // NativeUI.ShowToast(" + " + DiamondsPacksValue[3] + " Diamonds Added Successfull");
                break;


            //for KeyPack]
            case EM_IAPConstants.Product_Small_Pack_Key:
                Util.SetKey(Util.GetKey() + KeyPacksValue[0]);

#if UNITY_EDITOR
                print(" + " + KeyPacksValue[0] + " Keys Added Successfull");
#elif UNITY_ANDROID
            NativeUI.ShowToast(" + " + KeyPacksValue[0] + " Keys Added Successfull");
#elif UNITY_IPHONE || UNITY_IOS
            NativeUI.Alert("Process Success"," + " + KeyPacksValue[0] + " Keys Added Successfull","ok");
#endif

                //NativeUI.ShowToast(" + " + KeyPacksValue[0] + " Keys Added Successfull");
                break;
            case EM_IAPConstants.Product_Medium_Pack_key:
                Util.SetKey(Util.GetKey() + KeyPacksValue[1]);

#if UNITY_EDITOR
                print(" + " + KeyPacksValue[1] + " Keys Added Successfull");
#elif UNITY_ANDROID
            NativeUI.ShowToast(" + " + KeyPacksValue[1] + " Keys Added Successfull");
#elif UNITY_IPHONE || UNITY_IOS
            NativeUI.Alert("Process Success"," + " + KeyPacksValue[1] + " Keys Added Successfull","ok");
#endif

                //NativeUI.ShowToast(" + " + KeyPacksValue[1] + " Keys Added Successfull");
                break;
            case EM_IAPConstants.Product_Large_Pack_Key:
                Util.SetKey(Util.GetKey() + KeyPacksValue[2]);

#if UNITY_EDITOR
                print(" + " + KeyPacksValue[2] + " Keys Added Successfull");
#elif UNITY_ANDROID
            NativeUI.ShowToast(" + " + KeyPacksValue[2] + " Keys Added Successfull");
#elif UNITY_IPHONE || UNITY_IOS
            NativeUI.Alert("Process Success"," + " + KeyPacksValue[2] + " Keys Added Successfull","ok");
#endif

              
                break;
            case EM_IAPConstants.Product_Huge_Pack_Key:
                Util.SetKey(Util.GetKey() + KeyPacksValue[3]);

#if UNITY_EDITOR
                print(" + " + KeyPacksValue[3] + " Keys Added Successfull");
#elif UNITY_ANDROID
            NativeUI.ShowToast(" + " + KeyPacksValue[3] + " Keys Added Successfull");
#elif UNITY_IPHONE || UNITY_IOS
            NativeUI.Alert("Process Success"," + " + KeyPacksValue[3] + " Keys Added Successfull","ok");
#endif

                //NativeUI.ShowToast(" + " + KeyPacksValue[3] + " Keys Added Successfull");
                break;

            case EM_IAPConstants.Product_RemoveAds:

                PlayerPrefs.SetInt("RemoveAds",1);
                PlayerPrefs.Save();

#if UNITY_EDITOR
                print("Remove Ads Successfull");
#elif UNITY_ANDROID
            NativeUI.ShowToast("Remove Ads Successfull");
#elif UNITY_IPHONE || UNITY_IOS
            NativeUI.Alert("Process Success","Remove Ads Successfull","ok");
#endif

                break;
                // More products here...

        }

        MainScreenManager.mainScreenManagerRef.UpdateDiamondKeyText();
    }

    // Failed purchase handler
    void PurchaseFailedHandler(IAPProduct product)
    {
        Debug.Log("The purchase of product " + product.Name + " has failed.");
        //NativeUI.ShowToast("The purchase of product " + product.Name + " has failed.");

#if UNITY_EDITOR
        print("The purchase of product " + product.Name + " has failed.");
#elif UNITY_ANDROID
            NativeUI.ShowToast("The purchase of product " + product.Name + " has failed.");
#elif UNITY_IPHONE || UNITY_IOS
            NativeUI.Alert("Process Failed","The purchase of product " + product.Name + " has failed.","ok");
#endif
    }


    // Event handlers
    void OnUserLoginSucceeded()
    {
        Debug.Log("User logged in successfully. $>>>>>");
        // NativeUI.ShowToast("Signed In as : Success" );
    }

    void OnUserLoginFailed()
    {
        Debug.Log("User login failed. $>>>>>");
        // NativeUI.ShowToast("Signed In as : Failed");
    }
}
