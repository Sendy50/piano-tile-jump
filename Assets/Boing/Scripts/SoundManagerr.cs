﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerr : MonoBehaviour
{
    public static SoundManagerr Instance { get; private set; }

    public AudioSource _BgMusicSource;
    public AudioClip[] clips;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public AudioSource AudioSource
    {
        get
        {
            if (_BgMusicSource == null)
            {
                _BgMusicSource = GetComponent<AudioSource>();
            }

            return _BgMusicSource;
        }
    }

    public void PlayBgMusicInLoop()
    {
        //print("Sound Loop Method Call");

        if (PlayerPrefs.GetInt("Music", 1) == 1)
        {
            int randomIndex = Random.Range(0, clips.Length);

            _BgMusicSource.clip = clips[0];// randomIndex];
            _BgMusicSource.loop = true;
            _BgMusicSource.Play();
        }

    }
}