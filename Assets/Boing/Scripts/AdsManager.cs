﻿using AppAdvisory.Boing;
using EasyMobile;
//using GoogleMobileAdsMediationTestSuite.Api;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using GoogleMobileAds.Api.Mediation.UnityAds;


public class AdsManager : MonoBehaviour, IUnityAdsListener
{
    public static AdsManager Instance { get; private set; }
    private int Ad_id;

#if UNITY_IOS
    private string gameId = "3286004";
#elif UNITY_ANDROID
    private string gameId = "3286005";
#endif

    string myPlacementId = "rewardedVideo";
    string InterPlacementId = "video";
    bool testMode = true;
    private string placementIdd;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            //DontDestroyOnLoad(this.gameObject);
        }
        //else
        //{
        //    Destroy(gameObject);
        //}

        if (!RuntimeManager.IsInitialized())
            RuntimeManager.Init();


    }

    //Start is called before the first frame update
    void Start()
    {
        UnityAds.SetGDPRConsentMetaData(true);

        Advertisement.Initialize(gameId, testMode);
        // MediationTestSuite.OnMediationTestSuiteDismissed += this.HandleMediationTestSuiteDismissed;

        Advertisement.AddListener(this);
    }

    //private void HandleMediationTestSuiteDismissed(object sender, EventArgs e)
    //{
    //    Debug.Log("HandleMediationTestSuiteDismissed event received  $>>>>>");
    //}


    //void IUnityAdsListener.OnUnityAdsDidError(string message)
    //{
    //    print("AdManahger AdsShow OnUnityAdsDidError");
    //}

    //void IUnityAdsListener.OnUnityAdsDidStart(string placementId)
    //{
    //    print("AdManahger AdsShow OnUnityAdsDidStart");
    //}

    //private void HandleShowResult(ShowResult showResult)
    //{
    //    if (showResult == ShowResult.Finished)
    //    {
    //        print("AdManahger AdsShow Finished");

    //        //Paste That Code in Close Or Fail Ad Event
    //        if (Ad_id == 1)
    //        {
    //            StartCoroutine(GIftManager.giftManagerRef.ReturnToExtraRewardAdd());

    //        }
    //        else if (Ad_id == 2)
    //        {
    //            GIftManager.giftManagerRef.AfterAdAddGiftButton(); // add FreeGift When Gift == 0 
    //        }
    //        else if (Ad_id == 3)
    //        {
    //            ShopManager.ShopMangerRef.ReturnFromRewardAdX2Key();
    //        }
    //        else if (Ad_id == 4) //Watch & Play One Time Reward
    //        {
    //            MainScreenManager.mainScreenManagerRef.OpenLevel(PlayerPrefs.GetInt("LevelIndex"));
    //        }
    //        else if (Ad_id == 55)
    //        {
    //            MainScreenManager.mainScreenManagerRef.Add20Diamond();
    //        }
    //        else if (Ad_id == 66)
    //        {
    //            MainScreenManager.mainScreenManagerRef.Add2Key();
    //        }        
    //    }
    //    else if (showResult == ShowResult.Skipped)
    //    {
    //        print("AdManahger AdsShow Skipped");
    //    }
    //    else if (showResult == ShowResult.Failed)
    //    {
    //        print("AdManahger AdsShow Failed");
    //        NativeUI.ShowToast("Reward is not Ready", true);
    //    }
    //}

    //void IUnityAdsListener.OnUnityAdsReady(string placementId)
    //{
    //    print("AdManahger AdsShow OnUnityAdsReady " + placementId);
    //    placementIdd = placementId;
    //}

    // [System.Obsolete]



    public void ShowInterstialAds()
    {
        if (PlayerPrefs.GetInt("RemoveAds", 0) == 0)
        {
            if (Advertising.IsInterstitialAdReady())
            {
                Advertising.ShowInterstitialAd();
            }
            else
            {
                Advertisement.Show(InterPlacementId);
                print("Interstitial ads of unity");
            }
        }
        else
        {
            print("Interstitial ads not Show Because Remove Ads Purchased ...");
        }
    }


    private void InterstitialAdCompletedHandler(InterstitialAdNetwork arg1, AdPlacement arg2)
    {

    }




    public void ShowRewardAds(int id)
    {

        // Check if rewarded ad is ready
        bool isReady = Advertising.IsRewardedAdReady();

        // Show it if it's ready
        print("Is Ready : " + isReady);
        print("Ad_Id ???? " + id);
        Ad_id = id;

        if (isReady)
        {
            MainScreenManager.mainScreenManagerRef.PlayButtonClick();
            Advertising.ShowRewardedAd();
        }
        else
        {
            Advertisement.Show(myPlacementId);
        }
    }



    void OnEnable()
    {
        Advertising.RewardedAdCompleted += RewardedAdCompletedHandler;
        Advertising.RewardedAdSkipped += RewardedAdSkippedHandler;
        Advertising.InterstitialAdCompleted += InterstitialAdCompletedHandler;
    }

    void OnDisable()
    {
        Advertising.RewardedAdCompleted -= RewardedAdCompletedHandler;
        Advertising.RewardedAdSkipped -= RewardedAdSkippedHandler;
        Advertising.InterstitialAdCompleted -= InterstitialAdCompletedHandler;
    }


    // Event handler called when a rewarded ad has completed
    void RewardedAdCompletedHandler(RewardedAdNetwork network, AdLocation location)
    {
        Debug.Log("Rewarded ad has completed. The user should be rewarded now.");

        if (Ad_id == 1)
        {
            StartCoroutine(GIftManager.giftManagerRef.ReturnToExtraRewardAdd());

        }
        else if (Ad_id == 2)
        {
            GIftManager.giftManagerRef.AfterAdAddGiftButton(); // add FreeGift When Gift == 0 
        }
        else if (Ad_id == 3)
        {
            ShopManager.ShopMangerRef.ReturnFromRewardAdX2Key();
        }
        else if (Ad_id == 4) //Watch & Play One Time Reward
        {
            MainScreenManager.mainScreenManagerRef.OpenLevel(PlayerPrefs.GetInt("LevelIndex"));
        }
        else if (Ad_id == 55)
        {
            MainScreenManager.mainScreenManagerRef.Add20Diamond();
        }
        else if (Ad_id == 66)
        {
            MainScreenManager.mainScreenManagerRef.Add2Key();
        }
        else if (Ad_id == 77)
        {
            StartCoroutine(GIftManager.giftManagerRef.OpenBtnClick());
        }

    }

    // Event handler called when a rewarded ad has been skipped
    void RewardedAdSkippedHandler(RewardedAdNetwork network, AdLocation location)
    {
#if UNITY_EDITOR
        print("Reward isn't Available In Editor");
#elif UNITY_ANDROID
        NativeUI.ShowToast("Rewarded was Failed");
#elif UNITY_IPHONE || UNITY_IOS
        NativeUI.Alert("Reward Failed","Rewarded was Failed","ok");
#endif
    }


    public void OnUnityAdsReady(string placementId)
    {
        throw new NotImplementedException();
    }

    public void OnUnityAdsDidError(string message)
    {
#if UNITY_EDITOR
        print("Reward isn't Available In Editor");
#elif UNITY_ANDROID
        NativeUI.ShowToast("Reward isn't Available");
#elif UNITY_IPHONE || UNITY_IOS
        NativeUI.Alert("Reward Failed","Reward isn't Available","ok");
#endif
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        print("OnUnityAdsDidStart");
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {


        if (showResult == ShowResult.Finished)
        {
            if (placementId == myPlacementId)
            {
                Debug.Log("Rewarded ad has completed. The user should be rewarded now.");
                if (Ad_id == 1)
                {
                    StartCoroutine(GIftManager.giftManagerRef.ReturnToExtraRewardAdd());
                }
                else if (Ad_id == 2)
                {
                    GIftManager.giftManagerRef.AfterAdAddGiftButton(); // add FreeGift When Gift == 0 
                }
                else if (Ad_id == 3)
                {
                    ShopManager.ShopMangerRef.ReturnFromRewardAdX2Key();
                }
                else if (Ad_id == 4) //Watch & Play One Time Reward
                {
                    MainScreenManager.mainScreenManagerRef.OpenLevel(PlayerPrefs.GetInt("LevelIndex"));
                }
                else if (Ad_id == 55)
                {
                    MainScreenManager.mainScreenManagerRef.Add20Diamond();
                }
                else if (Ad_id == 66)
                {
                    MainScreenManager.mainScreenManagerRef.Add2Key();
                }
                else if (Ad_id == 77)
                {
                    StartCoroutine(GIftManager.giftManagerRef.OpenBtnClick());
                }
            }
            else
            {
                // Inter ads finish
                Debug.Log("Rewarded ad has completed. The user should be rewarded now.");
            }
        }
        else if (showResult == ShowResult.Skipped)
        {
            if (placementId == myPlacementId)
            {
#if UNITY_EDITOR
                print("Reward isn't Available In Editor");
#elif UNITY_ANDROID
                NativeUI.ShowToast("Rewarded was Failed");
#elif UNITY_IPHONE || UNITY_IOS
                NativeUI.Alert("Reward Failed","Rewarded was Failed","ok");
#endif
            }
        }
        else if (showResult == ShowResult.Failed)
        {
            if (placementId == myPlacementId)
            {
#if UNITY_EDITOR
                print("Reward isn't Available In Editor");
#elif UNITY_ANDROID
                NativeUI.ShowToast("Rewarded was Failed");
#elif UNITY_IPHONE || UNITY_IOS
                NativeUI.Alert("Reward Failed","Rewarded was Failed","ok");
#endif
            }
            else
            {
                print("Inter Failed...");
            }
        }
    }
}