﻿using UnityEngine;
using System.Collections;

namespace EasyMobile.Demo
{
    [RequireComponent(typeof(AudioSource))]
    public class SoundManager : MonoBehaviour
    {
        public static SoundManager Instance { get; private set; }

        public AudioClip button;
        private AudioSource _audioSource;

      

      

        void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public AudioSource AudioSource
        {
            get
            {
                if (_audioSource == null)
                {
                    _audioSource = GetComponent<AudioSource>();
                }

                return _audioSource;
            }
        }

        public void PlaySound(AudioClip sound)
        {
            AudioSource.PlayOneShot(sound);
        }
    }
}
