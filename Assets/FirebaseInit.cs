﻿using Firebase;
using Firebase.Analytics;
using Firebase.Messaging;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirebaseInit : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        print("############ Firebase Init Start #############");
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            var dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {

                FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
                FirebaseMessaging.TokenRegistrationOnInitEnabled = true;
                print("############ Firebase Init Success #############");

            }
            else
            {
                print("############ Firebase Init Failed #############");
                Debug.LogError(System.String.Format(
                "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
            }

        });
    }
}
